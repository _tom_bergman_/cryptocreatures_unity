pragma solidity ^0.5.10;

import "./CryptoMarket.sol";
//Work in Progress
contract CreatureGenerator is CryptoMarket {
    
    uint256 public genesOfCreature;
    //Placeholder value for maximum number of generation 0 creatures
    uint256 public constant MAX_GEN0 = 10;

    //Since gen 0 creatures go straight to auction, set how long it takes for auction to end
    uint64 public constant AUCTION_DURATION = 7 days;

    //To be compared to MAX_GEN0
    uint256 numberOfGen0Creatures;
    
    
    function createGen0Creature(uint256 _genes) public {
        require(numberOfGen0Creatures < MAX_GEN0, "");

        uint256 creatureId = createCreature(0, 0, 0, _genes, address(this));

        //Creates a sale for the creature. Gen0 creatures go straight to auction!
        marketplace.createSale(
            creatureId,
            address(this),
            gen0Price(),
            AUCTION_DURATION
        );

        numberOfGen0Creatures++;
    }

    function gen0Price() public returns(uint128) {
        //Placeholder until we figure out the economics involved
        return 100;
    }
    
    function getGenes(uint256 tokenId) public returns (uint256) {
        genesOfCreature = cCreatures[tokenId].genes;
        return genesOfCreature;
    }
}