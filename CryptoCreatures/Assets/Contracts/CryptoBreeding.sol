pragma solidity ^0.5.10;

contract CryptoBreeding {

    function breedable() internal view returns (bool) {
        //Ensure creatures have permission to breed. 
        //Returns true for now until we define the exact conditions under which a creature can breed
        return true;
    }
}