using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using Contracts.Contracts.CryptoCreatureCentral.ContractDefinition;

namespace Contracts.Contracts.CryptoCreatureCentral
{
    public partial class CryptoCreatureCentralService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, CryptoCreatureCentralDeployment cryptoCreatureCentralDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<CryptoCreatureCentralDeployment>().SendRequestAndWaitForReceiptAsync(cryptoCreatureCentralDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, CryptoCreatureCentralDeployment cryptoCreatureCentralDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<CryptoCreatureCentralDeployment>().SendRequestAsync(cryptoCreatureCentralDeployment);
        }

        public static async Task<CryptoCreatureCentralService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, CryptoCreatureCentralDeployment cryptoCreatureCentralDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, cryptoCreatureCentralDeployment, cancellationTokenSource);
            return new CryptoCreatureCentralService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public CryptoCreatureCentralService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<uint> BreedingCooldownsQueryAsync(BreedingCooldownsFunction breedingCooldownsFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<BreedingCooldownsFunction, uint>(breedingCooldownsFunction, blockParameter);
        }

        
        public Task<uint> BreedingCooldownsQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var breedingCooldownsFunction = new BreedingCooldownsFunction();
                breedingCooldownsFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<BreedingCooldownsFunction, uint>(breedingCooldownsFunction, blockParameter);
        }

        public Task<string> SetKingAddressRequestAsync(SetKingAddressFunction setKingAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(SetKingAddressFunction setKingAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<string> SetKingAddressRequestAsync(string newKing)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(string newKing, CancellationTokenSource cancellationToken = null)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<string> UnpauseRequestAsync(UnpauseFunction unpauseFunction)
        {
             return ContractHandler.SendRequestAsync(unpauseFunction);
        }

        public Task<string> UnpauseRequestAsync()
        {
             return ContractHandler.SendRequestAsync<UnpauseFunction>();
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(UnpauseFunction unpauseFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(unpauseFunction, cancellationToken);
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<UnpauseFunction>(null, cancellationToken);
        }

        public Task<string> PauseContractRequestAsync(PauseContractFunction pauseContractFunction)
        {
             return ContractHandler.SendRequestAsync(pauseContractFunction);
        }

        public Task<string> PauseContractRequestAsync()
        {
             return ContractHandler.SendRequestAsync<PauseContractFunction>();
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(PauseContractFunction pauseContractFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(pauseContractFunction, cancellationToken);
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<PauseContractFunction>(null, cancellationToken);
        }

        public Task<CCreaturesOutputDTO> CCreaturesQueryAsync(CCreaturesFunction cCreaturesFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryDeserializingToObjectAsync<CCreaturesFunction, CCreaturesOutputDTO>(cCreaturesFunction, blockParameter);
        }

        public Task<CCreaturesOutputDTO> CCreaturesQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var cCreaturesFunction = new CCreaturesFunction();
                cCreaturesFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryDeserializingToObjectAsync<CCreaturesFunction, CCreaturesOutputDTO>(cCreaturesFunction, blockParameter);
        }

        public Task<string> KingAddressQueryAsync(KingAddressFunction kingAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(kingAddressFunction, blockParameter);
        }

        
        public Task<string> KingAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(null, blockParameter);
        }

        public Task<bool> ContractPausedQueryAsync(ContractPausedFunction contractPausedFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(contractPausedFunction, blockParameter);
        }

        
        public Task<bool> ContractPausedQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(null, blockParameter);
        }

        public Task<string> MarketplaceQueryAsync(MarketplaceFunction marketplaceFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MarketplaceFunction, string>(marketplaceFunction, blockParameter);
        }

        
        public Task<string> MarketplaceQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MarketplaceFunction, string>(null, blockParameter);
        }

        public Task<string> CreateCreatureRequestAsync(CreateCreatureFunction createCreatureFunction)
        {
             return ContractHandler.SendRequestAsync(createCreatureFunction);
        }

        public Task<TransactionReceipt> CreateCreatureRequestAndWaitForReceiptAsync(CreateCreatureFunction createCreatureFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createCreatureFunction, cancellationToken);
        }

        public Task<string> CreateCreatureRequestAsync(BigInteger motherID, BigInteger fatherID, BigInteger generation, BigInteger genes, string owner)
        {
            var createCreatureFunction = new CreateCreatureFunction();
                createCreatureFunction.MotherID = motherID;
                createCreatureFunction.FatherID = fatherID;
                createCreatureFunction.Generation = generation;
                createCreatureFunction.Genes = genes;
                createCreatureFunction.Owner = owner;
            
             return ContractHandler.SendRequestAsync(createCreatureFunction);
        }

        public Task<TransactionReceipt> CreateCreatureRequestAndWaitForReceiptAsync(BigInteger motherID, BigInteger fatherID, BigInteger generation, BigInteger genes, string owner, CancellationTokenSource cancellationToken = null)
        {
            var createCreatureFunction = new CreateCreatureFunction();
                createCreatureFunction.MotherID = motherID;
                createCreatureFunction.FatherID = fatherID;
                createCreatureFunction.Generation = generation;
                createCreatureFunction.Genes = genes;
                createCreatureFunction.Owner = owner;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createCreatureFunction, cancellationToken);
        }

        public Task<string> SetGodAddressRequestAsync(SetGodAddressFunction setGodAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(SetGodAddressFunction setGodAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> SetGodAddressRequestAsync(string newGod)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(string newGod, CancellationTokenSource cancellationToken = null)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> CreatureIndexToOwnerQueryAsync(CreatureIndexToOwnerFunction creatureIndexToOwnerFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatureIndexToOwnerFunction, string>(creatureIndexToOwnerFunction, blockParameter);
        }

        
        public Task<string> CreatureIndexToOwnerQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var creatureIndexToOwnerFunction = new CreatureIndexToOwnerFunction();
                creatureIndexToOwnerFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<CreatureIndexToOwnerFunction, string>(creatureIndexToOwnerFunction, blockParameter);
        }

        public Task<string> CreatureIndexToApprovedQueryAsync(CreatureIndexToApprovedFunction creatureIndexToApprovedFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatureIndexToApprovedFunction, string>(creatureIndexToApprovedFunction, blockParameter);
        }

        
        public Task<string> CreatureIndexToApprovedQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var creatureIndexToApprovedFunction = new CreatureIndexToApprovedFunction();
                creatureIndexToApprovedFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<CreatureIndexToApprovedFunction, string>(creatureIndexToApprovedFunction, blockParameter);
        }

        public Task<string> GodAddressQueryAsync(GodAddressFunction godAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(godAddressFunction, blockParameter);
        }

        
        public Task<string> GodAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(null, blockParameter);
        }
    }
}
