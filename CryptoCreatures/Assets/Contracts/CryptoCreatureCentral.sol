pragma solidity ^0.5.10;

import "./CreatureControl.sol";
import "./Market.sol";
//CryptoCreatureCentral is just the base for the CrypoCreature contracts. We have other contracts (CreatureOwnership, CreatureGenerator etc.) that
//Inherit from this contract. This lowers the chance of us passing data incorrectly (costly) and creating more variables that will be
//Equivilent to the ones defined here.
contract CryptoCreatureCentral is CreatureControl {

    event Transfer(address from, address to, uint256 tokenID);
    event CreatureCreated(address owner, uint256 creatureID, uint256 genes);

    //CryptoCreature struct! This defines our CryptoCreature. The genes are the most essential part.
    //The other values are subject to change, but createdTime will give the user nice information about their creature.
    //breedingCooldown is important because we don't want people to spam the breeding functionality because it would be costly.
    //mother and fatherID is there because that's typically a requirement for breeding (Pokemon, Tamogotchi, etc) but we can change it.
    //generation is important for giving users an extra factor to consider when determining how valuable their creature is.
    struct CryptoCreature {
        uint256 genes;

        uint64 breedingCooldown;

        uint64 createdTime;

        uint32 motherID;

        uint32 fatherID;

        uint16 generation;
    }

    //Contains all of Market's functionality. Not sure if I will keep this here or move it down the hierarchy tree.
    Market public marketplace;

    //Constants that represent the range of possible cooldowns associated with breeding. These are the possible values we can assign breedingCooldown to
    //TO-DO Currently has placeholder values
    uint32[3] public breedingCooldowns = [
        uint32(1 days),
        uint32(2 days),
        uint32(3 days)
    ];

    CryptoCreature[] public cCreatures;

    //Maps CryptoCreatures to owner
    mapping (uint256 => address) public creatureIndexToOwner;

    mapping (address => uint256) ownershipTokenCount;

    mapping(uint256 => address) public creatureIndexToApproved;

    function transfer(address _from, address _to, uint256 _tokenID) internal {
            // Since the number of creatures is capped to 2^32 we can't overflow this
            ownershipTokenCount[_to]++;
            // transfer ownership
            creatureIndexToOwner[_tokenID] = _to;
            // When creating new creatures, _from is 0x0, but we can't account that address.
            if (_from != address(0)) {
                ownershipTokenCount[_from]--;
                // clear any previously approved ownership exchange
                delete creatureIndexToApproved[_tokenID];
            }
            // Emit the transfer event.
            emit Transfer(_from, _to, _tokenID);
        }

    //Requires the mother, father, generation number, genes and address of the owner
    function createCreature(
        uint256 _motherID,
        uint256 _fatherID,
        uint256 _generation,
        uint256 _genes,
        address _owner ) public returns (uint) {

        //Create a creature
        CryptoCreature memory _creature = CryptoCreature({
            genes: _genes,
            breedingCooldown: 0,
            createdTime: uint64(now),
            motherID: uint32(_motherID),
            fatherID: uint32(_fatherID),
            generation: uint16(_generation)
        });
        //Save it into the array. Forever!
        uint256 newCreatureID = cCreatures.push(_creature) - 1;

        //Log creature created
        emit CreatureCreated(
            _owner,
            newCreatureID,
            _creature.genes
        );

        //Transfers the creature to another address
        transfer(address(0), _owner, newCreatureID);
        return newCreatureID;
    }
    
}