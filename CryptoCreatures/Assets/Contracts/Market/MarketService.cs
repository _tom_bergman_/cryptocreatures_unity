using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using Contracts.Contracts.Market.ContractDefinition;

namespace Contracts.Contracts.Market
{
    public partial class MarketService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, MarketDeployment marketDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<MarketDeployment>().SendRequestAndWaitForReceiptAsync(marketDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, MarketDeployment marketDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<MarketDeployment>().SendRequestAsync(marketDeployment);
        }

        public static async Task<MarketService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, MarketDeployment marketDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, marketDeployment, cancellationTokenSource);
            return new MarketService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public MarketService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<string> CreatedQueryAsync(CreatedFunction createdFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatedFunction, string>(createdFunction, blockParameter);
        }

        
        public Task<string> CreatedQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatedFunction, string>(null, blockParameter);
        }

        public Task<string> RemoveSaleRequestAsync(RemoveSaleFunction removeSaleFunction)
        {
             return ContractHandler.SendRequestAsync(removeSaleFunction);
        }

        public Task<TransactionReceipt> RemoveSaleRequestAndWaitForReceiptAsync(RemoveSaleFunction removeSaleFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(removeSaleFunction, cancellationToken);
        }

        public Task<string> RemoveSaleRequestAsync(BigInteger tokenId)
        {
            var removeSaleFunction = new RemoveSaleFunction();
                removeSaleFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAsync(removeSaleFunction);
        }

        public Task<TransactionReceipt> RemoveSaleRequestAndWaitForReceiptAsync(BigInteger tokenId, CancellationTokenSource cancellationToken = null)
        {
            var removeSaleFunction = new RemoveSaleFunction();
                removeSaleFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(removeSaleFunction, cancellationToken);
        }

        public Task<string> NftQueryAsync(NftFunction nftFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NftFunction, string>(nftFunction, blockParameter);
        }

        
        public Task<string> NftQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NftFunction, string>(null, blockParameter);
        }

        public Task<string> CreateSaleRequestAsync(CreateSaleFunction createSaleFunction)
        {
             return ContractHandler.SendRequestAsync(createSaleFunction);
        }

        public Task<TransactionReceipt> CreateSaleRequestAndWaitForReceiptAsync(CreateSaleFunction createSaleFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createSaleFunction, cancellationToken);
        }

        public Task<string> CreateSaleRequestAsync(BigInteger tokenId, string addressOfSeller, BigInteger startPrice, ulong durationOfSale)
        {
            var createSaleFunction = new CreateSaleFunction();
                createSaleFunction.TokenId = tokenId;
                createSaleFunction.AddressOfSeller = addressOfSeller;
                createSaleFunction.StartPrice = startPrice;
                createSaleFunction.DurationOfSale = durationOfSale;
            
             return ContractHandler.SendRequestAsync(createSaleFunction);
        }

        public Task<TransactionReceipt> CreateSaleRequestAndWaitForReceiptAsync(BigInteger tokenId, string addressOfSeller, BigInteger startPrice, ulong durationOfSale, CancellationTokenSource cancellationToken = null)
        {
            var createSaleFunction = new CreateSaleFunction();
                createSaleFunction.TokenId = tokenId;
                createSaleFunction.AddressOfSeller = addressOfSeller;
                createSaleFunction.StartPrice = startPrice;
                createSaleFunction.DurationOfSale = durationOfSale;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createSaleFunction, cancellationToken);
        }

        public Task<string> BuyRequestAsync(BuyFunction buyFunction)
        {
             return ContractHandler.SendRequestAsync(buyFunction);
        }

        public Task<TransactionReceipt> BuyRequestAndWaitForReceiptAsync(BuyFunction buyFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(buyFunction, cancellationToken);
        }

        public Task<string> BuyRequestAsync(BigInteger tokenId, string addressOfBuyer)
        {
            var buyFunction = new BuyFunction();
                buyFunction.TokenId = tokenId;
                buyFunction.AddressOfBuyer = addressOfBuyer;
            
             return ContractHandler.SendRequestAsync(buyFunction);
        }

        public Task<TransactionReceipt> BuyRequestAndWaitForReceiptAsync(BigInteger tokenId, string addressOfBuyer, CancellationTokenSource cancellationToken = null)
        {
            var buyFunction = new BuyFunction();
                buyFunction.TokenId = tokenId;
                buyFunction.AddressOfBuyer = addressOfBuyer;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(buyFunction, cancellationToken);
        }

        public Task<string> TransferRequestAsync(TransferFunction transferFunction)
        {
             return ContractHandler.SendRequestAsync(transferFunction);
        }

        public Task<TransactionReceipt> TransferRequestAndWaitForReceiptAsync(TransferFunction transferFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFunction, cancellationToken);
        }

        public Task<string> TransferRequestAsync(BigInteger tokenId, string addressOfBuyer)
        {
            var transferFunction = new TransferFunction();
                transferFunction.TokenId = tokenId;
                transferFunction.AddressOfBuyer = addressOfBuyer;
            
             return ContractHandler.SendRequestAsync(transferFunction);
        }

        public Task<TransactionReceipt> TransferRequestAndWaitForReceiptAsync(BigInteger tokenId, string addressOfBuyer, CancellationTokenSource cancellationToken = null)
        {
            var transferFunction = new TransferFunction();
                transferFunction.TokenId = tokenId;
                transferFunction.AddressOfBuyer = addressOfBuyer;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFunction, cancellationToken);
        }
    }
}
