using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using Contracts.Contracts.CreatureGenerator.ContractDefinition;

namespace Contracts.Contracts.CreatureGenerator
{
    public partial class CreatureGeneratorService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, CreatureGeneratorDeployment creatureGeneratorDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<CreatureGeneratorDeployment>().SendRequestAndWaitForReceiptAsync(creatureGeneratorDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, CreatureGeneratorDeployment creatureGeneratorDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<CreatureGeneratorDeployment>().SendRequestAsync(creatureGeneratorDeployment);
        }

        public static async Task<CreatureGeneratorService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, CreatureGeneratorDeployment creatureGeneratorDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, creatureGeneratorDeployment, cancellationTokenSource);
            return new CreatureGeneratorService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public CreatureGeneratorService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<uint> BreedingCooldownsQueryAsync(BreedingCooldownsFunction breedingCooldownsFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<BreedingCooldownsFunction, uint>(breedingCooldownsFunction, blockParameter);
        }

        
        public Task<uint> BreedingCooldownsQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var breedingCooldownsFunction = new BreedingCooldownsFunction();
                breedingCooldownsFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<BreedingCooldownsFunction, uint>(breedingCooldownsFunction, blockParameter);
        }

        public Task<bool> SupportsInterfaceQueryAsync(SupportsInterfaceFunction supportsInterfaceFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<SupportsInterfaceFunction, bool>(supportsInterfaceFunction, blockParameter);
        }

        
        public Task<bool> SupportsInterfaceQueryAsync(byte[] interfaceID, BlockParameter blockParameter = null)
        {
            var supportsInterfaceFunction = new SupportsInterfaceFunction();
                supportsInterfaceFunction.InterfaceID = interfaceID;
            
            return ContractHandler.QueryAsync<SupportsInterfaceFunction, bool>(supportsInterfaceFunction, blockParameter);
        }

        public Task<string> NameQueryAsync(NameFunction nameFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NameFunction, string>(nameFunction, blockParameter);
        }

        
        public Task<string> NameQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NameFunction, string>(null, blockParameter);
        }

        public Task<string> ApproveRequestAsync(ApproveFunction approveFunction)
        {
             return ContractHandler.SendRequestAsync(approveFunction);
        }

        public Task<TransactionReceipt> ApproveRequestAndWaitForReceiptAsync(ApproveFunction approveFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(approveFunction, cancellationToken);
        }

        public Task<string> ApproveRequestAsync(string to, BigInteger tokenId)
        {
            var approveFunction = new ApproveFunction();
                approveFunction.To = to;
                approveFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAsync(approveFunction);
        }

        public Task<TransactionReceipt> ApproveRequestAndWaitForReceiptAsync(string to, BigInteger tokenId, CancellationTokenSource cancellationToken = null)
        {
            var approveFunction = new ApproveFunction();
                approveFunction.To = to;
                approveFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(approveFunction, cancellationToken);
        }

        public Task<string> SetKingAddressRequestAsync(SetKingAddressFunction setKingAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(SetKingAddressFunction setKingAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<string> SetKingAddressRequestAsync(string newKing)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(string newKing, CancellationTokenSource cancellationToken = null)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<BigInteger> TotalSupplyQueryAsync(TotalSupplyFunction totalSupplyFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<TotalSupplyFunction, BigInteger>(totalSupplyFunction, blockParameter);
        }

        
        public Task<BigInteger> TotalSupplyQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<TotalSupplyFunction, BigInteger>(null, blockParameter);
        }

        public Task<string> TransferFromRequestAsync(TransferFromFunction transferFromFunction)
        {
             return ContractHandler.SendRequestAsync(transferFromFunction);
        }

        public Task<TransactionReceipt> TransferFromRequestAndWaitForReceiptAsync(TransferFromFunction transferFromFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFromFunction, cancellationToken);
        }

        public Task<string> TransferFromRequestAsync(string from, string to, BigInteger tokenId)
        {
            var transferFromFunction = new TransferFromFunction();
                transferFromFunction.From = from;
                transferFromFunction.To = to;
                transferFromFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAsync(transferFromFunction);
        }

        public Task<TransactionReceipt> TransferFromRequestAndWaitForReceiptAsync(string from, string to, BigInteger tokenId, CancellationTokenSource cancellationToken = null)
        {
            var transferFromFunction = new TransferFromFunction();
                transferFromFunction.From = from;
                transferFromFunction.To = to;
                transferFromFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFromFunction, cancellationToken);
        }

        public Task<string> UnpauseRequestAsync(UnpauseFunction unpauseFunction)
        {
             return ContractHandler.SendRequestAsync(unpauseFunction);
        }

        public Task<string> UnpauseRequestAsync()
        {
             return ContractHandler.SendRequestAsync<UnpauseFunction>();
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(UnpauseFunction unpauseFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(unpauseFunction, cancellationToken);
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<UnpauseFunction>(null, cancellationToken);
        }

        public Task<string> PauseContractRequestAsync(PauseContractFunction pauseContractFunction)
        {
             return ContractHandler.SendRequestAsync(pauseContractFunction);
        }

        public Task<string> PauseContractRequestAsync()
        {
             return ContractHandler.SendRequestAsync<PauseContractFunction>();
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(PauseContractFunction pauseContractFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(pauseContractFunction, cancellationToken);
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<PauseContractFunction>(null, cancellationToken);
        }

        public Task<CCreaturesOutputDTO> CCreaturesQueryAsync(CCreaturesFunction cCreaturesFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryDeserializingToObjectAsync<CCreaturesFunction, CCreaturesOutputDTO>(cCreaturesFunction, blockParameter);
        }

        public Task<CCreaturesOutputDTO> CCreaturesQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var cCreaturesFunction = new CCreaturesFunction();
                cCreaturesFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryDeserializingToObjectAsync<CCreaturesFunction, CCreaturesOutputDTO>(cCreaturesFunction, blockParameter);
        }

        public Task<BigInteger> GenesOfCreatureQueryAsync(GenesOfCreatureFunction genesOfCreatureFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GenesOfCreatureFunction, BigInteger>(genesOfCreatureFunction, blockParameter);
        }

        
        public Task<BigInteger> GenesOfCreatureQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GenesOfCreatureFunction, BigInteger>(null, blockParameter);
        }

        public Task<string> OwnerOfQueryAsync(OwnerOfFunction ownerOfFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<OwnerOfFunction, string>(ownerOfFunction, blockParameter);
        }

        
        public Task<string> OwnerOfQueryAsync(BigInteger tokenId, BlockParameter blockParameter = null)
        {
            var ownerOfFunction = new OwnerOfFunction();
                ownerOfFunction.TokenId = tokenId;
            
            return ContractHandler.QueryAsync<OwnerOfFunction, string>(ownerOfFunction, blockParameter);
        }

        public Task<BigInteger> BalanceOfQueryAsync(BalanceOfFunction balanceOfFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<BalanceOfFunction, BigInteger>(balanceOfFunction, blockParameter);
        }

        
        public Task<BigInteger> BalanceOfQueryAsync(string owner, BlockParameter blockParameter = null)
        {
            var balanceOfFunction = new BalanceOfFunction();
                balanceOfFunction.Owner = owner;
            
            return ContractHandler.QueryAsync<BalanceOfFunction, BigInteger>(balanceOfFunction, blockParameter);
        }

        public Task<string> Gen0PriceRequestAsync(Gen0PriceFunction gen0PriceFunction)
        {
             return ContractHandler.SendRequestAsync(gen0PriceFunction);
        }

        public Task<string> Gen0PriceRequestAsync()
        {
             return ContractHandler.SendRequestAsync<Gen0PriceFunction>();
        }

        public Task<TransactionReceipt> Gen0PriceRequestAndWaitForReceiptAsync(Gen0PriceFunction gen0PriceFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(gen0PriceFunction, cancellationToken);
        }

        public Task<TransactionReceipt> Gen0PriceRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<Gen0PriceFunction>(null, cancellationToken);
        }

        public Task<string> KingAddressQueryAsync(KingAddressFunction kingAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(kingAddressFunction, blockParameter);
        }

        
        public Task<string> KingAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(null, blockParameter);
        }

        public Task<bool> ContractPausedQueryAsync(ContractPausedFunction contractPausedFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(contractPausedFunction, blockParameter);
        }

        
        public Task<bool> ContractPausedQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(null, blockParameter);
        }

        public Task<string> CreateGen0CreatureRequestAsync(CreateGen0CreatureFunction createGen0CreatureFunction)
        {
             return ContractHandler.SendRequestAsync(createGen0CreatureFunction);
        }

        public Task<TransactionReceipt> CreateGen0CreatureRequestAndWaitForReceiptAsync(CreateGen0CreatureFunction createGen0CreatureFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createGen0CreatureFunction, cancellationToken);
        }

        public Task<string> CreateGen0CreatureRequestAsync(BigInteger genes)
        {
            var createGen0CreatureFunction = new CreateGen0CreatureFunction();
                createGen0CreatureFunction.Genes = genes;
            
             return ContractHandler.SendRequestAsync(createGen0CreatureFunction);
        }

        public Task<TransactionReceipt> CreateGen0CreatureRequestAndWaitForReceiptAsync(BigInteger genes, CancellationTokenSource cancellationToken = null)
        {
            var createGen0CreatureFunction = new CreateGen0CreatureFunction();
                createGen0CreatureFunction.Genes = genes;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createGen0CreatureFunction, cancellationToken);
        }

        public Task<string> SymbolQueryAsync(SymbolFunction symbolFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<SymbolFunction, string>(symbolFunction, blockParameter);
        }

        
        public Task<string> SymbolQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<SymbolFunction, string>(null, blockParameter);
        }

        public Task<ulong> AUCTION_DURATIONQueryAsync(AUCTION_DURATIONFunction aUCTION_DURATIONFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<AUCTION_DURATIONFunction, ulong>(aUCTION_DURATIONFunction, blockParameter);
        }

        
        public Task<ulong> AUCTION_DURATIONQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<AUCTION_DURATIONFunction, ulong>(null, blockParameter);
        }

        public Task<string> GetGenesRequestAsync(GetGenesFunction getGenesFunction)
        {
             return ContractHandler.SendRequestAsync(getGenesFunction);
        }

        public Task<TransactionReceipt> GetGenesRequestAndWaitForReceiptAsync(GetGenesFunction getGenesFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(getGenesFunction, cancellationToken);
        }

        public Task<string> GetGenesRequestAsync(BigInteger tokenId)
        {
            var getGenesFunction = new GetGenesFunction();
                getGenesFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAsync(getGenesFunction);
        }

        public Task<TransactionReceipt> GetGenesRequestAndWaitForReceiptAsync(BigInteger tokenId, CancellationTokenSource cancellationToken = null)
        {
            var getGenesFunction = new GetGenesFunction();
                getGenesFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(getGenesFunction, cancellationToken);
        }

        public Task<string> TransferRequestAsync(TransferFunction transferFunction)
        {
             return ContractHandler.SendRequestAsync(transferFunction);
        }

        public Task<TransactionReceipt> TransferRequestAndWaitForReceiptAsync(TransferFunction transferFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFunction, cancellationToken);
        }

        public Task<string> TransferRequestAsync(string to, BigInteger tokenId)
        {
            var transferFunction = new TransferFunction();
                transferFunction.To = to;
                transferFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAsync(transferFunction);
        }

        public Task<TransactionReceipt> TransferRequestAndWaitForReceiptAsync(string to, BigInteger tokenId, CancellationTokenSource cancellationToken = null)
        {
            var transferFunction = new TransferFunction();
                transferFunction.To = to;
                transferFunction.TokenId = tokenId;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(transferFunction, cancellationToken);
        }

        public Task<string> MarketplaceQueryAsync(MarketplaceFunction marketplaceFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MarketplaceFunction, string>(marketplaceFunction, blockParameter);
        }

        
        public Task<string> MarketplaceQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MarketplaceFunction, string>(null, blockParameter);
        }

        public Task<string> CreateCreatureRequestAsync(CreateCreatureFunction createCreatureFunction)
        {
             return ContractHandler.SendRequestAsync(createCreatureFunction);
        }

        public Task<TransactionReceipt> CreateCreatureRequestAndWaitForReceiptAsync(CreateCreatureFunction createCreatureFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createCreatureFunction, cancellationToken);
        }

        public Task<string> CreateCreatureRequestAsync(BigInteger motherID, BigInteger fatherID, BigInteger generation, BigInteger genes, string owner)
        {
            var createCreatureFunction = new CreateCreatureFunction();
                createCreatureFunction.MotherID = motherID;
                createCreatureFunction.FatherID = fatherID;
                createCreatureFunction.Generation = generation;
                createCreatureFunction.Genes = genes;
                createCreatureFunction.Owner = owner;
            
             return ContractHandler.SendRequestAsync(createCreatureFunction);
        }

        public Task<TransactionReceipt> CreateCreatureRequestAndWaitForReceiptAsync(BigInteger motherID, BigInteger fatherID, BigInteger generation, BigInteger genes, string owner, CancellationTokenSource cancellationToken = null)
        {
            var createCreatureFunction = new CreateCreatureFunction();
                createCreatureFunction.MotherID = motherID;
                createCreatureFunction.FatherID = fatherID;
                createCreatureFunction.Generation = generation;
                createCreatureFunction.Genes = genes;
                createCreatureFunction.Owner = owner;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(createCreatureFunction, cancellationToken);
        }

        public Task<string> SetGodAddressRequestAsync(SetGodAddressFunction setGodAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(SetGodAddressFunction setGodAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> SetGodAddressRequestAsync(string newGod)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(string newGod, CancellationTokenSource cancellationToken = null)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> CreatureIndexToOwnerQueryAsync(CreatureIndexToOwnerFunction creatureIndexToOwnerFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatureIndexToOwnerFunction, string>(creatureIndexToOwnerFunction, blockParameter);
        }

        
        public Task<string> CreatureIndexToOwnerQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var creatureIndexToOwnerFunction = new CreatureIndexToOwnerFunction();
                creatureIndexToOwnerFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<CreatureIndexToOwnerFunction, string>(creatureIndexToOwnerFunction, blockParameter);
        }

        public Task<BigInteger> MAX_GEN0QueryAsync(MAX_GEN0Function mAX_GEN0Function, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MAX_GEN0Function, BigInteger>(mAX_GEN0Function, blockParameter);
        }

        
        public Task<BigInteger> MAX_GEN0QueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<MAX_GEN0Function, BigInteger>(null, blockParameter);
        }

        public Task<string> CreatureIndexToApprovedQueryAsync(CreatureIndexToApprovedFunction creatureIndexToApprovedFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<CreatureIndexToApprovedFunction, string>(creatureIndexToApprovedFunction, blockParameter);
        }

        
        public Task<string> CreatureIndexToApprovedQueryAsync(BigInteger returnValue1, BlockParameter blockParameter = null)
        {
            var creatureIndexToApprovedFunction = new CreatureIndexToApprovedFunction();
                creatureIndexToApprovedFunction.ReturnValue1 = returnValue1;
            
            return ContractHandler.QueryAsync<CreatureIndexToApprovedFunction, string>(creatureIndexToApprovedFunction, blockParameter);
        }

        public Task<string> GodAddressQueryAsync(GodAddressFunction godAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(godAddressFunction, blockParameter);
        }

        
        public Task<string> GodAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(null, blockParameter);
        }
    }
}
