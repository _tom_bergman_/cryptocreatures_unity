pragma solidity ^0.5.10;

import "./ERC721.sol";

//Contract that creates Sales (Creature that gets offered at a specific price)
contract Market {
    //Test string
    string public created = "A sale has not been created";

    //Sale struct
    struct Sale {
        address seller;

        uint128 price;

        uint startTime;

        uint64 durationOfSale;
    }

    //List of sale that are mapped to the creature's ID
    mapping (uint256 => Sale) tokenIdToSale;

    //Need this to utilize ERC functions
    ERC721 public nft;

    //See who owns the token. I've put this here as well because I imagine we will need to ensure ownership during a marketplace transaction
    function owns(address _owner, uint256 _tokenId) internal view returns (bool) {
        return (nft.ownerOf(_tokenId) == _owner);
    }

    //Adds creature to the mapping. Not sure if I should do it this way, but I imagined the key functionality in mappings would cost less gas. Time will tell
    function addSale(uint256 _tokenId, Sale memory _sale) internal {
        tokenIdToSale[_tokenId] = _sale;
    }

    //This is just a placeholder, but the function's future implementation will be simple. Create a Sale (struct) and use the addAuction function.
    //We will grab the Sale (struct) from the tokenIdToSale array in our mobile application and display it nicely to the user.
    //NOTE: We will have to look into caching this information effectively somewhere, because it is an expensive transaction to grab all of the Sales
    //One by one from the array!
    function createSale(uint256 _tokenId, address addressOfSeller, uint128 startPrice, uint64 durationOfSale) public {
        Sale memory newSale = Sale({
            seller: addressOfSeller,
            price: startPrice,
            startTime: now,
            durationOfSale: durationOfSale
        });
        addSale(_tokenId, newSale);
    }
    
    function transfer(uint256 tokenId, address addressOfBuyer) public {
        nft.transfer(addressOfBuyer, tokenId);
    }
    
    function buy(uint256 _tokenId, address addressOfBuyer) public {
        //Find Sale
        Sale memory sale = tokenIdToSale[_tokenId];
        //Require that the creature is on Sale 
        require(sale.price > 0, "Creature must be on sale before you can buy it");
        transfer(_tokenId, addressOfBuyer);
    }
    
    function removeSale(uint256 tokenId) public {
        delete tokenIdToSale[tokenId];
    }
}