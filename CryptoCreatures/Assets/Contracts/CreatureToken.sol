pragma solidity ^0.5.10;

import "./CryptoCreatureCentral.sol";
import "./ERC721.sol";
contract CreatureToken is CryptoCreatureCentral, ERC721 {
    //Name and Symbol are required by the ERC721 interface
    string public constant name = "CryptoCreatures";
    string public constant symbol = "CK";

    //Required for ERC-721 compliance - Work in Progress
    function supportsInterface(bytes4 _interfaceID) external view returns (bool) {
        return (_interfaceID == _interfaceID);
    }

    //Checks if the given address owns the given creature
    function owns(address _owner, uint256 _tokenId) internal view returns (bool) {
        return creatureIndexToOwner[_tokenId] == _owner;
    }

    //Searches based on tokenId of creature and checks to see if the owner matches what is returned
    function hasTransferApprovalFor(address _addressToApprove, uint256 _tokenId) internal view returns (bool) {
        return creatureIndexToApproved[_tokenId] == _addressToApprove;
    }

    function giveTransferApproval(address _addressToApprove, uint256 _tokenId) internal view {
        creatureIndexToApproved[_tokenId] == _addressToApprove;
    }

    //This function is required by ERC721
    function balanceOf(address _owner) public view returns (uint256 count) {
        return ownershipTokenCount[_owner];
    }

    //Transfers a creature to another address, whether it be the address of a user or a contract.
    //The reason we have a transfer function here as well as in the CryptoCreatureCentral contract is because we needed
    //A contract that satisfies the ERC721 implementation that is required to take use of ERC721 tokens.
    //Long story short, we use this function to ensure transfer isn't being abused or incorrectly used.
    function transfer(address _to, uint256 _tokenId) external notPaused {
        //Ensures creature isn't being sent to nowhere
        require(_to != address(0), "");

        //Ensures a creature isn't being sent to this address, which is just a waste of gas
        require(_to != address(this), "");

        //If we don't have this require, anyone can send any creature
        require(owns(msg.sender, _tokenId), "");

        transfer(msg.sender, _to, _tokenId);
    }

    function approve(address _to, uint256 _tokenId) external notPaused {
        //If we don't have this require, anyone can approve any creature
        require(owns(msg.sender, _tokenId), "");

        //Register approval
        creatureIndexToApproved[_tokenId] == _to;

        //Log the approval event which is located in the ERC721 contract
        emit Approval(msg.sender, _to, _tokenId);
    }

    //Transfer function used for when creatures already have transfer approval
    function transferFrom(address _from, address _to, uint256 _tokenId) external notPaused {
        //Ensures creature isn't being sent to nowhere
        require(_to != address(0), "");

        //Ensures creature isn't being sent to this address, waste of gas
        require(_to != address(this), "");

        require(hasTransferApprovalFor(msg.sender, _tokenId), "");

        require(owns(msg.sender, _tokenId), "");

        transfer(_from, _to, _tokenId);
    }

    //Required function for ERC-721 compliance
    //Returns total count of creatures that have been created so far
    function totalSupply() public view returns (uint) {
        return cCreatures.length - 1;
    }

    //Grabs owner of a given token (Creature)
    function ownerOf(uint256 _tokenId) external view returns (address) {
        require(creatureIndexToOwner[_tokenId] != address(0), "");

        return creatureIndexToOwner[_tokenId];
    }

}
    