pragma solidity ^0.5.10;

//Contract that defines the different kinds of contract owners as well as defines every modifier to be inheritted
//I'm not sure if we need this. I understand how it works, but we need to look into how we maintain our contracts. It's here for now regardless, better safe then sorry

contract CreatureControl {

    //Two random levels of access. I have seen this alongside the ability to pause in a few other ethereum applications, both of which seem to be required if we were to "upgrade" a contract
    //Or to change its address. Not sure if we will ever do this, but it's here for now.
    address public godAddress;
    address public kingAddress;

    bool public contractPaused = false;

    //Following "onlyX" modifiers can be used in other functions to ensure it is being executed by the right user. E.G. function egFunction() external onlyX {code}
    //Checks if user is god
    modifier onlyGod() {
        require(msg.sender == godAddress, "");
        _;
    }
    //Checks if user is a king
    modifier onlyKing() {
        require(msg.sender == kingAddress, "");
        _;
    }

    //Checks if user is any of the above
    modifier onlyRoyalty() {
        require(
            msg.sender == godAddress ||
            msg.sender == kingAddress
        );
        _;
    }

    //Changes god address to the one defined in the parameter
    function setGodAddress(address _newGod) external onlyGod {
        require(_newGod != address(0), "");

        godAddress = _newGod;
    }

    //Changes king address to the one defined in the parameter
    function setKingAddress(address _newKing) external onlyKing {
        require(_newKing != address(0), "");

        kingAddress = _newKing;
    }

    //When contract isn't paused
    modifier notPaused() {
        require(!contractPaused, "");
        _;
    }
    //When contract is paused
    modifier whenPaused() {
        require(contractPaused, "");
        _;
    }
    //Pauses contract
    function pauseContract() external onlyRoyalty notPaused {
        contractPaused = true;
    }
    //Unpauses contract
    function unpause() public onlyKing whenPaused {
        contractPaused = false;
    }
}