using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using Contracts.Contracts.CreatureControl.ContractDefinition;

namespace Contracts.Contracts.CreatureControl
{
    public partial class CreatureControlService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, CreatureControlDeployment creatureControlDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<CreatureControlDeployment>().SendRequestAndWaitForReceiptAsync(creatureControlDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, CreatureControlDeployment creatureControlDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<CreatureControlDeployment>().SendRequestAsync(creatureControlDeployment);
        }

        public static async Task<CreatureControlService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, CreatureControlDeployment creatureControlDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, creatureControlDeployment, cancellationTokenSource);
            return new CreatureControlService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public CreatureControlService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<string> SetKingAddressRequestAsync(SetKingAddressFunction setKingAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(SetKingAddressFunction setKingAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<string> SetKingAddressRequestAsync(string newKing)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAsync(setKingAddressFunction);
        }

        public Task<TransactionReceipt> SetKingAddressRequestAndWaitForReceiptAsync(string newKing, CancellationTokenSource cancellationToken = null)
        {
            var setKingAddressFunction = new SetKingAddressFunction();
                setKingAddressFunction.NewKing = newKing;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setKingAddressFunction, cancellationToken);
        }

        public Task<string> UnpauseRequestAsync(UnpauseFunction unpauseFunction)
        {
             return ContractHandler.SendRequestAsync(unpauseFunction);
        }

        public Task<string> UnpauseRequestAsync()
        {
             return ContractHandler.SendRequestAsync<UnpauseFunction>();
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(UnpauseFunction unpauseFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(unpauseFunction, cancellationToken);
        }

        public Task<TransactionReceipt> UnpauseRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<UnpauseFunction>(null, cancellationToken);
        }

        public Task<string> PauseContractRequestAsync(PauseContractFunction pauseContractFunction)
        {
             return ContractHandler.SendRequestAsync(pauseContractFunction);
        }

        public Task<string> PauseContractRequestAsync()
        {
             return ContractHandler.SendRequestAsync<PauseContractFunction>();
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(PauseContractFunction pauseContractFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(pauseContractFunction, cancellationToken);
        }

        public Task<TransactionReceipt> PauseContractRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<PauseContractFunction>(null, cancellationToken);
        }

        public Task<string> KingAddressQueryAsync(KingAddressFunction kingAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(kingAddressFunction, blockParameter);
        }

        
        public Task<string> KingAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<KingAddressFunction, string>(null, blockParameter);
        }

        public Task<bool> ContractPausedQueryAsync(ContractPausedFunction contractPausedFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(contractPausedFunction, blockParameter);
        }

        
        public Task<bool> ContractPausedQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ContractPausedFunction, bool>(null, blockParameter);
        }

        public Task<string> SetGodAddressRequestAsync(SetGodAddressFunction setGodAddressFunction)
        {
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(SetGodAddressFunction setGodAddressFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> SetGodAddressRequestAsync(string newGod)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAsync(setGodAddressFunction);
        }

        public Task<TransactionReceipt> SetGodAddressRequestAndWaitForReceiptAsync(string newGod, CancellationTokenSource cancellationToken = null)
        {
            var setGodAddressFunction = new SetGodAddressFunction();
                setGodAddressFunction.NewGod = newGod;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(setGodAddressFunction, cancellationToken);
        }

        public Task<string> GodAddressQueryAsync(GodAddressFunction godAddressFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(godAddressFunction, blockParameter);
        }

        
        public Task<string> GodAddressQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GodAddressFunction, string>(null, blockParameter);
        }
    }
}
