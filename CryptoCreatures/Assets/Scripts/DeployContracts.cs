﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.ABI.Model;
using Nethereum.Contracts;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.Extensions;
using Nethereum.JsonRpc.UnityClient;
using UnityEngine;

public class DeployContracts : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DeployContractRequest();
    }

    public void DeployContractRequest()
    {
        StartCoroutine(DeployTheContracts());
    }

    public IEnumerator DeployTheContracts()
    {
        var url = "http://localhost:8545";
        var privateKey = "0xb5b1870957d373ef0eeffecc6e4812c0fd08f554b37b233526acc331bf1544f7";
        var account = "0x12890D2cce102216644c59daE5baed380d84830c";
        //initialising the transaction request sender
        var transactionRequest = new TransactionSignedUnityRequest(url, privateKey);

        var deployCreatureGeneratorContract = new Contracts.Contracts.CreatureGenerator.ContractDefinition.CreatureGeneratorDeployment()
        {
            FromAddress = account
        };

        //deploy the contract and True indicates we want to estimate the gas
        yield return transactionRequest.SignAndSendDeploymentContractTransaction<Contracts.Contracts.CreatureGenerator.ContractDefinition.CreatureGeneratorDeploymentBase>(deployCreatureGeneratorContract);

        if (transactionRequest.Exception != null)
        {
            Debug.Log(transactionRequest.Exception.Message);
            yield break;
        }

        var transactionHash = transactionRequest.Result;

        Debug.Log("Deployment transaction hash:" + transactionHash);

        //create a poll to get the receipt when mined
        var transactionReceiptPolling = new TransactionReceiptPollingRequest(url);
        //checking every 2 seconds for the receipt
        yield return transactionReceiptPolling.PollForReceipt(transactionHash, 2);
        var deploymentReceipt = transactionReceiptPolling.Result;

        Debug.Log("Deployment contract address:" + deploymentReceipt.ContractAddress);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
