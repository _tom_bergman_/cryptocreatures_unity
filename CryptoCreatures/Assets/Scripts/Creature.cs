﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour {

	[SerializeField]
	private int _hunger;
	[SerializeField]
	private int _happiness;
	[SerializeField]
	private string _name;

	private bool _serverTime;
	private int _touchCount;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetString("then", "28 July 2019 7:32:47 AM");
		updateStats ();
		if (!PlayerPrefs.HasKey ("name"))
			PlayerPrefs.SetString ("name", "Barry");
		_name = PlayerPrefs.GetString ("name");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);

			if (touch.phase == TouchPhase.Began) {
				Debug.Log ("Touched");
				Vector2 v = touch.position;
				RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (v), Vector2.zero);
				if (hit) {
					Debug.Log (hit.transform.gameObject.name);
					if (hit.transform.gameObject.tag == "creature") {
						_touchCount++;
						if (_touchCount >= 3) {
							_touchCount = 0;
							updateHappiness (1);
						}
					}
				}
			}
		}


		/*EnforceBounds ();*/

		// Use this for testing clicking 
		/*
		if (Input.GetMouseButtonUp (0)) {
			Debug.Log ("Clicked");
			Vector2 v = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (v), Vector2.zero);
			if (hit) {
				Debug.Log (hit.transform.gameObject.name);
				if (hit.transform.gameObject.tag == "creature") {
					_touchCount++;
					if (_touchCount >= 3) {
						_touchCount = 0;
						updateHappiness (1);
					}
				}
			}
		}
		*/

	}

	void updateStats() {

		if (!PlayerPrefs.HasKey ("_hunger")) {
			_hunger = 100;
			PlayerPrefs.SetInt("_hunger", _hunger);
		} else {
			_hunger = PlayerPrefs.GetInt ("_hunger");
		}

		if (!PlayerPrefs.HasKey("_happiness")) {
			_happiness = 100;
			PlayerPrefs.SetInt ("_happiness", _happiness);
		} else {
			_happiness = PlayerPrefs.GetInt ("_happiness");
		}

		if (!PlayerPrefs.HasKey ("then"))
			PlayerPrefs.SetString ("then", getStringTime ());

		TimeSpan ts = getTimeSpan ();

		_hunger -= (int)(ts.TotalHours * 2);
		if (_hunger < 0)
			_hunger = 0;
		_happiness -= (int)((100 - _hunger) * (ts.TotalHours / 5));
		if (_happiness < 0)
			_happiness = 0;

		//Debug.Log(getTimeSpan().ToString());
		//Debug.Log(getTimeSpan().TotalHours);

		if (_serverTime)
			updateServer ();
		else
			InvokeRepeating ("updateDevice", 0f, 30f);
	}

	void updateServer() {
	}

	void updateDevice() {
		PlayerPrefs.SetString ("then", getStringTime ());
	}

	string getStringTime() {
		DateTime now = DateTime.Now;
		return now.Day + "/" + now.Month + "/" + now.Year + " " + now.Hour + ":" + now.Minute + ":" + now.Second;
	}

	TimeSpan getTimeSpan() {
		if (_serverTime)
			return new TimeSpan ();
		else
			return DateTime.Now - Convert.ToDateTime(PlayerPrefs.GetString ("then"));
	}

	public int hunger {
		get{ return _hunger; }
		set{ _hunger = value; }
	}

	public int happiness {
		get{ return _happiness; }
		set{ _happiness = value; }
	}

	public string name {
		get{ return _name; }
		set{ _name = value; }

	}

	public void updateHappiness (int i) {
		happiness += i;
			if (happiness > 100)
				_happiness = 100;
	}

	public void saveCreature() {
		if (!_serverTime)
			updateDevice ();
		PlayerPrefs.SetInt ("_hunger", _hunger);
		PlayerPrefs.SetInt ("_happiness", _happiness);
	}

	private void EnforceBounds()
	{
		// 1
		Vector3 newPosition = transform.position; 
		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;

		// 2
		float xDist = mainCamera.aspect * mainCamera.orthographicSize; 
		float xMax = cameraPosition.x + xDist;
		float xMin = cameraPosition.x - xDist;

		// 3
		if ( newPosition.x < xMin || newPosition.x > xMax ) {
			newPosition.x = Mathf.Clamp( newPosition.x, xMin, xMax );
		}

		float yMax = mainCamera.orthographicSize;

		if (newPosition.y < -yMax || newPosition.y > yMax) {
			newPosition.y = Mathf.Clamp( newPosition.y, -yMax, yMax );
		}

		// 4
		transform.position = newPosition;
	}

}