﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject hungerText;
	public GameObject happinessText;

	public GameObject namePanel;
	public GameObject nameInput;
	public GameObject nameText;

	public GameObject creature;

	public GameObject creaturePanel;
	public GameObject creatureHead;
	public Sprite[] headOptionSprites;
	public GameObject[] headOptions;

	public GameObject homePanel;
	public Sprite[] homeTileSprites;
	public GameObject[] homeTiles;

	public GameObject background;
	public Sprite[] backgroundOptions;

	public GameObject foodPanel;
	public Sprite[] foodIcons;

	void Start() {
		if (!PlayerPrefs.HasKey ("tiles"))
			PlayerPrefs.SetInt ("tiles", 0);
		changeTiles(PlayerPrefs.GetInt("tiles"));

		if (!PlayerPrefs.HasKey ("background"))
			PlayerPrefs.SetInt ("background", 0);
		changeBackground (PlayerPrefs.GetInt ("background"));

		if (!PlayerPrefs.HasKey ("head"))
			PlayerPrefs.SetInt ("head", 0);
		changeHead (PlayerPrefs.GetInt ("head"));
	}

	// Update is called once per frame
	void Update () {
		hungerText.GetComponent<Text> ().text = "" + creature.GetComponent<Creature> ().hunger;
		happinessText.GetComponent<Text> ().text = "" + creature.GetComponent<Creature> ().happiness;
		nameText.GetComponent<Text> ().text = creature.GetComponent<Creature> ().name;
	}

	public void triggerNamePanel(bool b) {
		namePanel.SetActive (!namePanel.activeInHierarchy);

		if (b) {
			creature.GetComponent<Creature> ().name = nameInput.GetComponent<InputField> ().text;
			PlayerPrefs.SetString ("name", creature.GetComponent<Creature> ().name);
		}
	}

	public void buttonBehaviour(int i) {
		switch (i) {
		case(0):
		default:

			break;
		case(1):
			homePanel.SetActive (!homePanel.activeInHierarchy);
			break;

		case(2):
			foodPanel.SetActive (!foodPanel.activeInHierarchy);
			break;

		case(3):
			creaturePanel.SetActive (!creaturePanel.activeInHierarchy);
			break;

		case(4):
			creature.GetComponent<Creature> ().saveCreature ();
			Application.Quit ();
			break;
		}

	}

	public void changeTiles(int t) {
		for (int i = 0; i < homeTiles.Length; i++) {
			homeTiles [i].GetComponent<SpriteRenderer> ().sprite = homeTileSprites [t];
		}

		toggle (homePanel);

		PlayerPrefs.SetInt ("tiles", t);
	}

	public void changeBackground (int i) {
		background.GetComponent<SpriteRenderer> ().sprite = backgroundOptions [i];

		toggle (homePanel);

		PlayerPrefs.SetInt ("background", i);
	}

	public void changeHead (int i) {
		creatureHead.GetComponent<SpriteRenderer> ().sprite = headOptionSprites [i];

		toggle (creaturePanel);

		PlayerPrefs.SetInt ("head", i);
		
	}


	public void selectFood (int i ) {
		toggle (foodPanel);
	}

	public void toggle (GameObject g) {
		if (g.activeInHierarchy)
			g.SetActive (false);
	}
}
