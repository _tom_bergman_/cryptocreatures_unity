using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.CreatureControl.ContractDefinition
{


    public partial class CreatureControlDeployment : CreatureControlDeploymentBase
    {
        public CreatureControlDeployment() : base(BYTECODE) { }
        public CreatureControlDeployment(string byteCode) : base(byteCode) { }
    }

    public class CreatureControlDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "60806040526001805460ff60a01b1916905534801561001d57600080fd5b506103a58061002d6000396000f3fe608060405234801561001057600080fd5b506004361061007d5760003560e01c80638289d8d51161005b5780638289d8d5146100ba5780638a67456a146100de578063c181cecb146100fa578063f8900ddd146101205761007d565b80630e8fc228146100825780633f4ba83a146100aa578063439766ce146100b2575b600080fd5b6100a86004803603602081101561009857600080fd5b50356001600160a01b0316610128565b005b6100a86101b8565b6100a8610238565b6100c26102b2565b604080516001600160a01b039092168252519081900360200190f35b6100e66102c1565b604080519115158252519081900360200190f35b6100a86004803603602081101561011057600080fd5b50356001600160a01b03166102d1565b6100c2610361565b6001546001600160a01b03163314610161576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610196576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600180546001600160a01b0319166001600160a01b0392909216919091179055565b6001546001600160a01b031633146101f1576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600154600160a01b900460ff16610229576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b19169055565b6000546001600160a01b031633148061025b57506001546001600160a01b031633145b61026457600080fd5b600154600160a01b900460ff161561029d576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b1916600160a01b179055565b6001546001600160a01b031681565b600154600160a01b900460ff1681565b6000546001600160a01b0316331461030a576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b03811661033f576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600080546001600160a01b0319166001600160a01b0392909216919091179055565b6000546001600160a01b03168156fea265627a7a723058201bbe6b4725b3790fe3f11d426e9a97338bedeec0fe2b1bd5b398916c383ee5fe64736f6c634300050a0032";
        public CreatureControlDeploymentBase() : base(BYTECODE) { }
        public CreatureControlDeploymentBase(string byteCode) : base(byteCode) { }

    }

    public partial class SetKingAddressFunction : SetKingAddressFunctionBase { }

    [Function("setKingAddress")]
    public class SetKingAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newKing", 1)]
        public virtual string NewKing { get; set; }
    }

    public partial class UnpauseFunction : UnpauseFunctionBase { }

    [Function("unpause")]
    public class UnpauseFunctionBase : FunctionMessage
    {

    }

    public partial class PauseContractFunction : PauseContractFunctionBase { }

    [Function("pauseContract")]
    public class PauseContractFunctionBase : FunctionMessage
    {

    }

    public partial class KingAddressFunction : KingAddressFunctionBase { }

    [Function("kingAddress", "address")]
    public class KingAddressFunctionBase : FunctionMessage
    {

    }

    public partial class ContractPausedFunction : ContractPausedFunctionBase { }

    [Function("contractPaused", "bool")]
    public class ContractPausedFunctionBase : FunctionMessage
    {

    }

    public partial class SetGodAddressFunction : SetGodAddressFunctionBase { }

    [Function("setGodAddress")]
    public class SetGodAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newGod", 1)]
        public virtual string NewGod { get; set; }
    }

    public partial class GodAddressFunction : GodAddressFunctionBase { }

    [Function("godAddress", "address")]
    public class GodAddressFunctionBase : FunctionMessage
    {

    }







    public partial class KingAddressOutputDTO : KingAddressOutputDTOBase { }

    [FunctionOutput]
    public class KingAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class ContractPausedOutputDTO : ContractPausedOutputDTOBase { }

    [FunctionOutput]
    public class ContractPausedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }



    public partial class GodAddressOutputDTO : GodAddressOutputDTOBase { }

    [FunctionOutput]
    public class GodAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }
}
