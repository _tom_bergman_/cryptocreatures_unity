using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.CreatureToken.ContractDefinition
{


    public partial class CreatureTokenDeployment : CreatureTokenDeploymentBase
    {
        public CreatureTokenDeployment() : base(BYTECODE) { }
        public CreatureTokenDeployment(string byteCode) : base(byteCode) { }
    }

    public class CreatureTokenDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "6001805460ff60a01b1916905560e06040526201518060809081526202a30060a0526203f48060c0526100359060039081610048565b5034801561004257600080fd5b50610108565b6001830191839082156100d45791602002820160005b838211156100a257835183826101000a81548163ffffffff021916908363ffffffff160217905550926020019260040160208160030104928301926001030261005e565b80156100d25782816101000a81549063ffffffff02191690556004016020816003010492830192600103026100a2565b505b506100e09291506100e4565b5090565b61010591905b808211156100e057805463ffffffff191681556001016100ea565b90565b610ec8806101176000396000f3fe608060405234801561001057600080fd5b50600436106101425760003560e01c806370a08231116100b8578063abc8c7af1161007c578063abc8c7af146103f2578063bdc593fe146103fa578063c181cecb14610438578063dafa55a11461045e578063ee0b1e091461047b578063f8900ddd1461049857610142565b806370a08231146103885780638289d8d5146103ae5780638a67456a146103b657806395d89b41146103be578063a9059cbb146103c657610142565b806318160ddd1161010a57806318160ddd1461028957806323b872dd146102a35780633f4ba83a146102d9578063439766ce146102e157806349195467146102e95780636352211e1461034f57610142565b8063016a4e0d1461014757806301ffc9a71461017d57806306fdde03146101b8578063095ea7b3146102355780630e8fc22814610263575b600080fd5b6101646004803603602081101561015d57600080fd5b50356104a0565b6040805163ffffffff9092168252519081900360200190f35b6101a46004803603602081101561019357600080fd5b50356001600160e01b0319166104cd565b604080519115158252519081900360200190f35b6101c06104d3565b6040805160208082528351818301528351919283929083019185019080838360005b838110156101fa5781810151838201526020016101e2565b50505050905090810190601f1680156102275780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6102616004803603604081101561024b57600080fd5b506001600160a01b0381351690602001356104fe565b005b6102616004803603602081101561027957600080fd5b50356001600160a01b03166105c5565b610291610655565b60408051918252519081900360200190f35b610261600480360360608110156102b957600080fd5b506001600160a01b0381358116916020810135909116906040013561065f565b61026161077f565b6102616107ff565b610306600480360360208110156102ff57600080fd5b5035610879565b6040805196875267ffffffffffffffff9586166020880152939094168584015263ffffffff918216606086015216608084015261ffff90911660a0830152519081900360c00190f35b61036c6004803603602081101561036557600080fd5b50356108e7565b604080516001600160a01b039092168252519081900360200190f35b6102916004803603602081101561039e57600080fd5b50356001600160a01b0316610946565b61036c610961565b6101a4610970565b6101c0610980565b610261600480360360408110156103dc57600080fd5b506001600160a01b03813516906020013561099e565b61036c610a88565b610291600480360360a081101561041057600080fd5b50803590602081013590604081013590606081013590608001356001600160a01b0316610a97565b6102616004803603602081101561044e57600080fd5b50356001600160a01b0316610c79565b61036c6004803603602081101561047457600080fd5b5035610d09565b61036c6004803603602081101561049157600080fd5b5035610d24565b61036c610d3f565b600381600381106104ad57fe5b60089182820401919006600402915054906101000a900463ffffffff1681565b50600190565b6040518060400160405280600f81526020016e43727970746f43726561747572657360881b81525081565b600154600160a01b900460ff1615610537576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6105413382610d4e565b61056c576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600081905260076020908152604080513381526001600160a01b03851692810192909252818101839052517f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259181900360600190a15050565b6001546001600160a01b031633146105fe576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610633576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600180546001600160a01b0319166001600160a01b0392909216919091179055565b6004546000190190565b600154600160a01b900460ff1615610698576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b0382166106cd576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038216301415610705576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b61070f3382610d6e565b61073a576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6107443382610d4e565b61076f576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b61077a838383610d8e565b505050565b6001546001600160a01b031633146107b8576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600154600160a01b900460ff166107f0576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b19169055565b6000546001600160a01b031633148061082257506001546001600160a01b031633145b61082b57600080fd5b600154600160a01b900460ff1615610864576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b1916600160a01b179055565b6004818154811061088657fe5b60009182526020909120600290910201805460019091015490915067ffffffffffffffff808216916801000000000000000081049091169063ffffffff600160801b8204811691600160a01b81049091169061ffff600160c01b9091041686565b6000818152600560205260408120546001600160a01b031661092a576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b506000908152600560205260409020546001600160a01b031690565b6001600160a01b031660009081526006602052604090205490565b6001546001600160a01b031681565b600154600160a01b900460ff1681565b60405180604001604052806002815260200161434b60f01b81525081565b600154600160a01b900460ff16156109d7576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038216610a0c576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038216301415610a44576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b610a4e3382610d4e565b610a79576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b610a84338383610d8e565b5050565b6002546001600160a01b031681565b6000610aa1610e5e565b6040518060c00160405280858152602001600067ffffffffffffffff1681526020014267ffffffffffffffff1681526020018863ffffffff1681526020018763ffffffff1681526020018661ffff16815250905060006001600483908060018154018082558091505090600182039060005260206000209060020201600090919290919091506000820151816000015560208201518160010160006101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060408201518160010160086101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060608201518160010160106101000a81548163ffffffff021916908363ffffffff16021790555060808201518160010160146101000a81548163ffffffff021916908363ffffffff16021790555060a08201518160010160186101000a81548161ffff021916908361ffff16021790555050500390507f58a3e712a89de27edeb9517f0adeb8aa16c75a478ac5197facfc848f82ec34f38482846000015160405180846001600160a01b03166001600160a01b03168152602001838152602001828152602001935050505060405180910390a1610c6e60008583610d8e565b979650505050505050565b6000546001600160a01b03163314610cb2576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610ce7576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600080546001600160a01b0319166001600160a01b0392909216919091179055565b6005602052600090815260409020546001600160a01b031681565b6007602052600090815260409020546001600160a01b031681565b6000546001600160a01b031681565b6000908152600560205260409020546001600160a01b0391821691161490565b6000908152600760205260409020546001600160a01b0391821691161490565b6001600160a01b038083166000818152600660209081526040808320805460010190558583526005909152902080546001600160a01b0319169091179055831615610e0f576001600160a01b038316600090815260066020908152604080832080546000190190558383526007909152902080546001600160a01b03191690555b604080516001600160a01b0380861682528416602082015280820183905290517fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9181900360600190a1505050565b6040805160c081018252600080825260208201819052918101829052606081018290526080810182905260a08101919091529056fea265627a7a723058201e86dcf1433956956bef268603f4fdda7adbb49917aec032111ab9035cc4233764736f6c634300050a0032";
        public CreatureTokenDeploymentBase() : base(BYTECODE) { }
        public CreatureTokenDeploymentBase(string byteCode) : base(byteCode) { }

    }

    public partial class BreedingCooldownsFunction : BreedingCooldownsFunctionBase { }

    [Function("breedingCooldowns", "uint32")]
    public class BreedingCooldownsFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class SupportsInterfaceFunction : SupportsInterfaceFunctionBase { }

    [Function("supportsInterface", "bool")]
    public class SupportsInterfaceFunctionBase : FunctionMessage
    {
        [Parameter("bytes4", "_interfaceID", 1)]
        public virtual byte[] InterfaceID { get; set; }
    }

    public partial class NameFunction : NameFunctionBase { }

    [Function("name", "string")]
    public class NameFunctionBase : FunctionMessage
    {

    }

    public partial class ApproveFunction : ApproveFunctionBase { }

    [Function("approve")]
    public class ApproveFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 2)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class SetKingAddressFunction : SetKingAddressFunctionBase { }

    [Function("setKingAddress")]
    public class SetKingAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newKing", 1)]
        public virtual string NewKing { get; set; }
    }

    public partial class TotalSupplyFunction : TotalSupplyFunctionBase { }

    [Function("totalSupply", "uint256")]
    public class TotalSupplyFunctionBase : FunctionMessage
    {

    }

    public partial class TransferFromFunction : TransferFromFunctionBase { }

    [Function("transferFrom")]
    public class TransferFromFunctionBase : FunctionMessage
    {
        [Parameter("address", "_from", 1)]
        public virtual string From { get; set; }
        [Parameter("address", "_to", 2)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 3)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class UnpauseFunction : UnpauseFunctionBase { }

    [Function("unpause")]
    public class UnpauseFunctionBase : FunctionMessage
    {

    }

    public partial class PauseContractFunction : PauseContractFunctionBase { }

    [Function("pauseContract")]
    public class PauseContractFunctionBase : FunctionMessage
    {

    }

    public partial class CCreaturesFunction : CCreaturesFunctionBase { }

    [Function("cCreatures", typeof(CCreaturesOutputDTO))]
    public class CCreaturesFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class OwnerOfFunction : OwnerOfFunctionBase { }

    [Function("ownerOf", "address")]
    public class OwnerOfFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class BalanceOfFunction : BalanceOfFunctionBase { }

    [Function("balanceOf", "uint256")]
    public class BalanceOfFunctionBase : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public virtual string Owner { get; set; }
    }

    public partial class KingAddressFunction : KingAddressFunctionBase { }

    [Function("kingAddress", "address")]
    public class KingAddressFunctionBase : FunctionMessage
    {

    }

    public partial class ContractPausedFunction : ContractPausedFunctionBase { }

    [Function("contractPaused", "bool")]
    public class ContractPausedFunctionBase : FunctionMessage
    {

    }

    public partial class SymbolFunction : SymbolFunctionBase { }

    [Function("symbol", "string")]
    public class SymbolFunctionBase : FunctionMessage
    {

    }

    public partial class TransferFunction : TransferFunctionBase { }

    [Function("transfer")]
    public class TransferFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 2)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class MarketplaceFunction : MarketplaceFunctionBase { }

    [Function("marketplace", "address")]
    public class MarketplaceFunctionBase : FunctionMessage
    {

    }

    public partial class CreateCreatureFunction : CreateCreatureFunctionBase { }

    [Function("createCreature", "uint256")]
    public class CreateCreatureFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_motherID", 1)]
        public virtual BigInteger MotherID { get; set; }
        [Parameter("uint256", "_fatherID", 2)]
        public virtual BigInteger FatherID { get; set; }
        [Parameter("uint256", "_generation", 3)]
        public virtual BigInteger Generation { get; set; }
        [Parameter("uint256", "_genes", 4)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("address", "_owner", 5)]
        public virtual string Owner { get; set; }
    }

    public partial class SetGodAddressFunction : SetGodAddressFunctionBase { }

    [Function("setGodAddress")]
    public class SetGodAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newGod", 1)]
        public virtual string NewGod { get; set; }
    }

    public partial class CreatureIndexToOwnerFunction : CreatureIndexToOwnerFunctionBase { }

    [Function("creatureIndexToOwner", "address")]
    public class CreatureIndexToOwnerFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class CreatureIndexToApprovedFunction : CreatureIndexToApprovedFunctionBase { }

    [Function("creatureIndexToApproved", "address")]
    public class CreatureIndexToApprovedFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class GodAddressFunction : GodAddressFunctionBase { }

    [Function("godAddress", "address")]
    public class GodAddressFunctionBase : FunctionMessage
    {

    }

    public partial class TransferEventDTO : TransferEventDTOBase { }

    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {
        [Parameter("address", "from", 1, false )]
        public virtual string From { get; set; }
        [Parameter("address", "to", 2, false )]
        public virtual string To { get; set; }
        [Parameter("uint256", "tokenId", 3, false )]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class ApprovalEventDTO : ApprovalEventDTOBase { }

    [Event("Approval")]
    public class ApprovalEventDTOBase : IEventDTO
    {
        [Parameter("address", "owner", 1, false )]
        public virtual string Owner { get; set; }
        [Parameter("address", "approved", 2, false )]
        public virtual string Approved { get; set; }
        [Parameter("uint256", "tokenId", 3, false )]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class CreatureCreatedEventDTO : CreatureCreatedEventDTOBase { }

    [Event("CreatureCreated")]
    public class CreatureCreatedEventDTOBase : IEventDTO
    {
        [Parameter("address", "owner", 1, false )]
        public virtual string Owner { get; set; }
        [Parameter("uint256", "creatureID", 2, false )]
        public virtual BigInteger CreatureID { get; set; }
        [Parameter("uint256", "genes", 3, false )]
        public virtual BigInteger Genes { get; set; }
    }

    public partial class BreedingCooldownsOutputDTO : BreedingCooldownsOutputDTOBase { }

    [FunctionOutput]
    public class BreedingCooldownsOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint32", "", 1)]
        public virtual uint ReturnValue1 { get; set; }
    }

    public partial class SupportsInterfaceOutputDTO : SupportsInterfaceOutputDTOBase { }

    [FunctionOutput]
    public class SupportsInterfaceOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }

    public partial class NameOutputDTO : NameOutputDTOBase { }

    [FunctionOutput]
    public class NameOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("string", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }





    public partial class TotalSupplyOutputDTO : TotalSupplyOutputDTOBase { }

    [FunctionOutput]
    public class TotalSupplyOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }







    public partial class CCreaturesOutputDTO : CCreaturesOutputDTOBase { }

    [FunctionOutput]
    public class CCreaturesOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "genes", 1)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("uint64", "breedingCooldown", 2)]
        public virtual ulong BreedingCooldown { get; set; }
        [Parameter("uint64", "createdTime", 3)]
        public virtual ulong CreatedTime { get; set; }
        [Parameter("uint32", "motherID", 4)]
        public virtual uint MotherID { get; set; }
        [Parameter("uint32", "fatherID", 5)]
        public virtual uint FatherID { get; set; }
        [Parameter("uint16", "generation", 6)]
        public virtual ushort Generation { get; set; }
    }

    public partial class OwnerOfOutputDTO : OwnerOfOutputDTOBase { }

    [FunctionOutput]
    public class OwnerOfOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class BalanceOfOutputDTO : BalanceOfOutputDTOBase { }

    [FunctionOutput]
    public class BalanceOfOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "count", 1)]
        public virtual BigInteger Count { get; set; }
    }

    public partial class KingAddressOutputDTO : KingAddressOutputDTOBase { }

    [FunctionOutput]
    public class KingAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class ContractPausedOutputDTO : ContractPausedOutputDTOBase { }

    [FunctionOutput]
    public class ContractPausedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }

    public partial class SymbolOutputDTO : SymbolOutputDTOBase { }

    [FunctionOutput]
    public class SymbolOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("string", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }



    public partial class MarketplaceOutputDTO : MarketplaceOutputDTOBase { }

    [FunctionOutput]
    public class MarketplaceOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }





    public partial class CreatureIndexToOwnerOutputDTO : CreatureIndexToOwnerOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToOwnerOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class CreatureIndexToApprovedOutputDTO : CreatureIndexToApprovedOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToApprovedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class GodAddressOutputDTO : GodAddressOutputDTOBase { }

    [FunctionOutput]
    public class GodAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }
}
