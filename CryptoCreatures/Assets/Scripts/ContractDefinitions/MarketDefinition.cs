using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.Market.ContractDefinition
{


    public partial class MarketDeployment : MarketDeploymentBase
    {
        public MarketDeployment() : base(BYTECODE) { }
        public MarketDeployment(string byteCode) : base(byteCode) { }
    }

    public class MarketDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "60c0604052601b60808190527f412073616c6520686173206e6f74206265656e2063726561746564000000000060a090815261003e9160009190610051565b5034801561004b57600080fd5b506100ec565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061009257805160ff19168380011785556100bf565b828001600101855582156100bf579182015b828111156100bf5782518255916020019190600101906100a4565b506100cb9291506100cf565b5090565b6100e991905b808211156100cb57600081556001016100d5565b90565b61053a806100fb6000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c8063325a19f11461006757806344e797e9146100e457806347ccca02146101035780636528ab07146101275780637deb602514610172578063b7760c8f1461019e575b600080fd5b61006f6101ca565b6040805160208082528351818301528351919283929083019185019080838360005b838110156100a9578181015183820152602001610091565b50505050905090810190601f1680156100d65780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b610101600480360360208110156100fa57600080fd5b5035610258565b005b61010b6102a3565b604080516001600160a01b039092168252519081900360200190f35b6101016004803603608081101561013d57600080fd5b5080359060208101356001600160a01b03169060408101356001600160801b0316906060013567ffffffffffffffff166102b2565b6101016004803603604081101561018857600080fd5b50803590602001356001600160a01b031661030a565b610101600480360360408110156101b457600080fd5b50803590602001356001600160a01b03166103b9565b6000805460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815292918301828280156102505780601f1061022557610100808354040283529160200191610250565b820191906000526020600020905b81548152906001019060200180831161023357829003601f168201915b505050505081565b6000908152600160208190526040822080546001600160a01b031916815590810180546001600160801b03191690556002810191909155600301805467ffffffffffffffff19169055565b6002546001600160a01b031681565b6102ba6104b0565b6040518060800160405280856001600160a01b03168152602001846001600160801b031681526020014281526020018367ffffffffffffffff168152509050610303858261042a565b5050505050565b6103126104b0565b50600082815260016020818152604092839020835160808101855281546001600160a01b03168152928101546001600160801b031691830182905260028101549383019390935260039092015467ffffffffffffffff166060820152906103aa5760405162461bcd60e51b815260040180806020018281038252602e8152602001806104d8602e913960400191505060405180910390fd5b6103b483836103b9565b505050565b6002546040805163a9059cbb60e01b81526001600160a01b038481166004830152602482018690529151919092169163a9059cbb91604480830192600092919082900301818387803b15801561040e57600080fd5b505af1158015610422573d6000803e3d6000fd5b505050505050565b600091825260016020818152604093849020835181546001600160a01b0319166001600160a01b039091161781559083015191810180546001600160801b0319166001600160801b0390931692909217909155918101516002830155606001516003909101805467ffffffffffffffff191667ffffffffffffffff909216919091179055565b6040805160808101825260008082526020820181905291810182905260608101919091529056fe4372656174757265206d757374206265206f6e2073616c65206265666f726520796f752063616e20627579206974a265627a7a723058208feeada3c5c2f5a05bdf99f9b5f3b64ff5b6f23ae4a166759efe8aa05caa207264736f6c634300050a0032";
        public MarketDeploymentBase() : base(BYTECODE) { }
        public MarketDeploymentBase(string byteCode) : base(byteCode) { }

    }

    public partial class CreatedFunction : CreatedFunctionBase { }

    [Function("created", "string")]
    public class CreatedFunctionBase : FunctionMessage
    {

    }

    public partial class RemoveSaleFunction : RemoveSaleFunctionBase { }

    [Function("removeSale")]
    public class RemoveSaleFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class NftFunction : NftFunctionBase { }

    [Function("nft", "address")]
    public class NftFunctionBase : FunctionMessage
    {

    }

    public partial class CreateSaleFunction : CreateSaleFunctionBase { }

    [Function("createSale")]
    public class CreateSaleFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
        [Parameter("address", "addressOfSeller", 2)]
        public virtual string AddressOfSeller { get; set; }
        [Parameter("uint128", "startPrice", 3)]
        public virtual BigInteger StartPrice { get; set; }
        [Parameter("uint64", "durationOfSale", 4)]
        public virtual ulong DurationOfSale { get; set; }
    }

    public partial class BuyFunction : BuyFunctionBase { }

    [Function("buy")]
    public class BuyFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
        [Parameter("address", "addressOfBuyer", 2)]
        public virtual string AddressOfBuyer { get; set; }
    }

    public partial class TransferFunction : TransferFunctionBase { }

    [Function("transfer")]
    public class TransferFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
        [Parameter("address", "addressOfBuyer", 2)]
        public virtual string AddressOfBuyer { get; set; }
    }

    public partial class CreatedOutputDTO : CreatedOutputDTOBase { }

    [FunctionOutput]
    public class CreatedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("string", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }



    public partial class NftOutputDTO : NftOutputDTOBase { }

    [FunctionOutput]
    public class NftOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }






}
