using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.CryptoCreatureCentral.ContractDefinition
{


    public partial class CryptoCreatureCentralDeployment : CryptoCreatureCentralDeploymentBase
    {
        public CryptoCreatureCentralDeployment() : base(BYTECODE) { }
        public CryptoCreatureCentralDeployment(string byteCode) : base(byteCode) { }
    }

    public class CryptoCreatureCentralDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "6001805460ff60a01b1916905560e06040526201518060809081526202a30060a0526203f48060c0526100359060039081610048565b5034801561004257600080fd5b50610108565b6001830191839082156100d45791602002820160005b838211156100a257835183826101000a81548163ffffffff021916908363ffffffff160217905550926020019260040160208160030104928301926001030261005e565b80156100d25782816101000a81549063ffffffff02191690556004016020816003010492830192600103026100a2565b505b506100e09291506100e4565b5090565b61010591905b808211156100e057805463ffffffff191681556001016100ea565b90565b6108ec806101176000396000f3fe608060405234801561001057600080fd5b50600436106100cf5760003560e01c80638a67456a1161008c578063c181cecb11610066578063c181cecb14610240578063dafa55a114610266578063ee0b1e0914610283578063f8900ddd146102a0576100cf565b80638a67456a146101cc578063abc8c7af146101e8578063bdc593fe146101f0576100cf565b8063016a4e0d146100d45780630e8fc2281461010a5780633f4ba83a14610132578063439766ce1461013a57806349195467146101425780638289d8d5146101a8575b600080fd5b6100f1600480360360208110156100ea57600080fd5b50356102a8565b6040805163ffffffff9092168252519081900360200190f35b6101306004803603602081101561012057600080fd5b50356001600160a01b03166102d5565b005b610130610365565b6101306103e5565b61015f6004803603602081101561015857600080fd5b503561045f565b6040805196875267ffffffffffffffff9586166020880152939094168584015263ffffffff918216606086015216608084015261ffff90911660a0830152519081900360c00190f35b6101b06104cd565b604080516001600160a01b039092168252519081900360200190f35b6101d46104dc565b604080519115158252519081900360200190f35b6101b06104ec565b61022e600480360360a081101561020657600080fd5b50803590602081013590604081013590606081013590608001356001600160a01b03166104fb565b60408051918252519081900360200190f35b6101306004803603602081101561025657600080fd5b50356001600160a01b03166106dd565b6101b06004803603602081101561027c57600080fd5b503561076d565b6101b06004803603602081101561029957600080fd5b5035610788565b6101b06107a3565b600381600381106102b557fe5b60089182820401919006600402915054906101000a900463ffffffff1681565b6001546001600160a01b0316331461030e576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610343576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600180546001600160a01b0319166001600160a01b0392909216919091179055565b6001546001600160a01b0316331461039e576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600154600160a01b900460ff166103d6576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b19169055565b6000546001600160a01b031633148061040857506001546001600160a01b031633145b61041157600080fd5b600154600160a01b900460ff161561044a576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b1916600160a01b179055565b6004818154811061046c57fe5b60009182526020909120600290910201805460019091015490915067ffffffffffffffff808216916801000000000000000081049091169063ffffffff600160801b8204811691600160a01b81049091169061ffff600160c01b9091041686565b6001546001600160a01b031681565b600154600160a01b900460ff1681565b6002546001600160a01b031681565b6000610505610882565b6040518060c00160405280858152602001600067ffffffffffffffff1681526020014267ffffffffffffffff1681526020018863ffffffff1681526020018763ffffffff1681526020018661ffff16815250905060006001600483908060018154018082558091505090600182039060005260206000209060020201600090919290919091506000820151816000015560208201518160010160006101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060408201518160010160086101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060608201518160010160106101000a81548163ffffffff021916908363ffffffff16021790555060808201518160010160146101000a81548163ffffffff021916908363ffffffff16021790555060a08201518160010160186101000a81548161ffff021916908361ffff16021790555050500390507f58a3e712a89de27edeb9517f0adeb8aa16c75a478ac5197facfc848f82ec34f38482846000015160405180846001600160a01b03166001600160a01b03168152602001838152602001828152602001935050505060405180910390a16106d2600085836107b2565b979650505050505050565b6000546001600160a01b03163314610716576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b03811661074b576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600080546001600160a01b0319166001600160a01b0392909216919091179055565b6005602052600090815260409020546001600160a01b031681565b6007602052600090815260409020546001600160a01b031681565b6000546001600160a01b031681565b6001600160a01b038083166000818152600660209081526040808320805460010190558583526005909152902080546001600160a01b0319169091179055831615610833576001600160a01b038316600090815260066020908152604080832080546000190190558383526007909152902080546001600160a01b03191690555b604080516001600160a01b0380861682528416602082015280820183905290517fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9181900360600190a1505050565b6040805160c081018252600080825260208201819052918101829052606081018290526080810182905260a08101919091529056fea265627a7a72305820de32dc12c10008afac7eac3089c0304ce0603104017d5802d33e44d79c2b033864736f6c634300050a0032";
        public CryptoCreatureCentralDeploymentBase() : base(BYTECODE) { }
        public CryptoCreatureCentralDeploymentBase(string byteCode) : base(byteCode) { }

    }

    public partial class BreedingCooldownsFunction : BreedingCooldownsFunctionBase { }

    [Function("breedingCooldowns", "uint32")]
    public class BreedingCooldownsFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class SetKingAddressFunction : SetKingAddressFunctionBase { }

    [Function("setKingAddress")]
    public class SetKingAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newKing", 1)]
        public virtual string NewKing { get; set; }
    }

    public partial class UnpauseFunction : UnpauseFunctionBase { }

    [Function("unpause")]
    public class UnpauseFunctionBase : FunctionMessage
    {

    }

    public partial class PauseContractFunction : PauseContractFunctionBase { }

    [Function("pauseContract")]
    public class PauseContractFunctionBase : FunctionMessage
    {

    }

    public partial class CCreaturesFunction : CCreaturesFunctionBase { }

    [Function("cCreatures", typeof(CCreaturesOutputDTO))]
    public class CCreaturesFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class KingAddressFunction : KingAddressFunctionBase { }

    [Function("kingAddress", "address")]
    public class KingAddressFunctionBase : FunctionMessage
    {

    }

    public partial class ContractPausedFunction : ContractPausedFunctionBase { }

    [Function("contractPaused", "bool")]
    public class ContractPausedFunctionBase : FunctionMessage
    {

    }

    public partial class MarketplaceFunction : MarketplaceFunctionBase { }

    [Function("marketplace", "address")]
    public class MarketplaceFunctionBase : FunctionMessage
    {

    }

    public partial class CreateCreatureFunction : CreateCreatureFunctionBase { }

    [Function("createCreature", "uint256")]
    public class CreateCreatureFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_motherID", 1)]
        public virtual BigInteger MotherID { get; set; }
        [Parameter("uint256", "_fatherID", 2)]
        public virtual BigInteger FatherID { get; set; }
        [Parameter("uint256", "_generation", 3)]
        public virtual BigInteger Generation { get; set; }
        [Parameter("uint256", "_genes", 4)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("address", "_owner", 5)]
        public virtual string Owner { get; set; }
    }

    public partial class SetGodAddressFunction : SetGodAddressFunctionBase { }

    [Function("setGodAddress")]
    public class SetGodAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newGod", 1)]
        public virtual string NewGod { get; set; }
    }

    public partial class CreatureIndexToOwnerFunction : CreatureIndexToOwnerFunctionBase { }

    [Function("creatureIndexToOwner", "address")]
    public class CreatureIndexToOwnerFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class CreatureIndexToApprovedFunction : CreatureIndexToApprovedFunctionBase { }

    [Function("creatureIndexToApproved", "address")]
    public class CreatureIndexToApprovedFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class GodAddressFunction : GodAddressFunctionBase { }

    [Function("godAddress", "address")]
    public class GodAddressFunctionBase : FunctionMessage
    {

    }

    public partial class TransferEventDTO : TransferEventDTOBase { }

    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {
        [Parameter("address", "from", 1, false )]
        public virtual string From { get; set; }
        [Parameter("address", "to", 2, false )]
        public virtual string To { get; set; }
        [Parameter("uint256", "tokenID", 3, false )]
        public virtual BigInteger TokenID { get; set; }
    }

    public partial class CreatureCreatedEventDTO : CreatureCreatedEventDTOBase { }

    [Event("CreatureCreated")]
    public class CreatureCreatedEventDTOBase : IEventDTO
    {
        [Parameter("address", "owner", 1, false )]
        public virtual string Owner { get; set; }
        [Parameter("uint256", "creatureID", 2, false )]
        public virtual BigInteger CreatureID { get; set; }
        [Parameter("uint256", "genes", 3, false )]
        public virtual BigInteger Genes { get; set; }
    }

    public partial class BreedingCooldownsOutputDTO : BreedingCooldownsOutputDTOBase { }

    [FunctionOutput]
    public class BreedingCooldownsOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint32", "", 1)]
        public virtual uint ReturnValue1 { get; set; }
    }







    public partial class CCreaturesOutputDTO : CCreaturesOutputDTOBase { }

    [FunctionOutput]
    public class CCreaturesOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "genes", 1)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("uint64", "breedingCooldown", 2)]
        public virtual ulong BreedingCooldown { get; set; }
        [Parameter("uint64", "createdTime", 3)]
        public virtual ulong CreatedTime { get; set; }
        [Parameter("uint32", "motherID", 4)]
        public virtual uint MotherID { get; set; }
        [Parameter("uint32", "fatherID", 5)]
        public virtual uint FatherID { get; set; }
        [Parameter("uint16", "generation", 6)]
        public virtual ushort Generation { get; set; }
    }

    public partial class KingAddressOutputDTO : KingAddressOutputDTOBase { }

    [FunctionOutput]
    public class KingAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class ContractPausedOutputDTO : ContractPausedOutputDTOBase { }

    [FunctionOutput]
    public class ContractPausedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }

    public partial class MarketplaceOutputDTO : MarketplaceOutputDTOBase { }

    [FunctionOutput]
    public class MarketplaceOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }





    public partial class CreatureIndexToOwnerOutputDTO : CreatureIndexToOwnerOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToOwnerOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class CreatureIndexToApprovedOutputDTO : CreatureIndexToApprovedOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToApprovedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class GodAddressOutputDTO : GodAddressOutputDTOBase { }

    [FunctionOutput]
    public class GodAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }
}
