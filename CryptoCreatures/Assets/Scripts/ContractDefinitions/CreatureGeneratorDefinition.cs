using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.CreatureGenerator.ContractDefinition
{


    public partial class CreatureGeneratorDeployment : CreatureGeneratorDeploymentBase
    {
        public CreatureGeneratorDeployment() : base(BYTECODE) { }
        public CreatureGeneratorDeployment(string byteCode) : base(byteCode) { }
    }

    public class CreatureGeneratorDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "6001805460ff60a01b1916905560e06040526201518060809081526202a30060a0526203f48060c052610035906003908161003b565b506100fb565b6001830191839082156100c75791602002820160005b8382111561009557835183826101000a81548163ffffffff021916908363ffffffff1602179055509260200192600401602081600301049283019260010302610051565b80156100c55782816101000a81549063ffffffff0219169055600401602081600301049283019260010302610095565b505b506100d39291506100d7565b5090565b6100f891905b808211156100d357805463ffffffff191681556001016100dd565b90565b6111088061010a6000396000f3fe608060405234801561001057600080fd5b50600436106101c45760003560e01c80638289d8d5116100f9578063abc8c7af11610097578063dafa55a111610071578063dafa55a11461056b578063de2cb10a14610588578063ee0b1e0914610590578063f8900ddd146105ad576101c4565b8063abc8c7af146104ff578063bdc593fe14610507578063c181cecb14610545576101c4565b806395d89b41116100d357806395d89b4114610489578063a271e52514610491578063a2e4b12e146104b6578063a9059cbb146104d3576101c4565b80638289d8d51461045c5780638a67456a1461046457806394ad79331461046c576101c4565b80633f4ba83a1161016657806360aca1981161014057806360aca198146103d15780636352211e146103d957806370a08231146104125780637d9b6a1514610438576101c4565b80633f4ba83a1461035b578063439766ce14610363578063491954671461036b576101c4565b8063095ea7b3116101a2578063095ea7b3146102b75780630e8fc228146102e557806318160ddd1461030b57806323b872dd14610325576101c4565b8063016a4e0d146101c957806301ffc9a7146101ff57806306fdde031461023a575b600080fd5b6101e6600480360360208110156101df57600080fd5b50356105b5565b6040805163ffffffff9092168252519081900360200190f35b6102266004803603602081101561021557600080fd5b50356001600160e01b0319166105e2565b604080519115158252519081900360200190f35b6102426105e8565b6040805160208082528351818301528351919283929083019185019080838360005b8381101561027c578181015183820152602001610264565b50505050905090810190601f1680156102a95780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6102e3600480360360408110156102cd57600080fd5b506001600160a01b038135169060200135610613565b005b6102e3600480360360208110156102fb57600080fd5b50356001600160a01b03166106da565b61031361076a565b60408051918252519081900360200190f35b6102e36004803603606081101561033b57600080fd5b506001600160a01b03813581169160208101359091169060400135610774565b6102e3610894565b6102e3610914565b6103886004803603602081101561038157600080fd5b503561098e565b6040805196875267ffffffffffffffff9586166020880152939094168584015263ffffffff918216606086015216608084015261ffff90911660a0830152519081900360c00190f35b6103136109fc565b6103f6600480360360208110156103ef57600080fd5b5035610a02565b604080516001600160a01b039092168252519081900360200190f35b6103136004803603602081101561042857600080fd5b50356001600160a01b0316610a61565b610440610a7c565b604080516001600160801b039092168252519081900360200190f35b6103f6610a81565b610226610a90565b6102e36004803603602081101561048257600080fd5b5035610aa0565b610242610b89565b610499610ba7565b6040805167ffffffffffffffff9092168252519081900360200190f35b610313600480360360208110156104cc57600080fd5b5035610bae565b6102e3600480360360408110156104e957600080fd5b506001600160a01b038135169060200135610bd9565b6103f6610cc3565b610313600480360360a081101561051d57600080fd5b50803590602081013590604081013590606081013590608001356001600160a01b0316610cd2565b6102e36004803603602081101561055b57600080fd5b50356001600160a01b0316610eb4565b6103f66004803603602081101561058157600080fd5b5035610f44565b610313610f5f565b6103f6600480360360208110156105a657600080fd5b5035610f64565b6103f6610f7f565b600381600381106105c257fe5b60089182820401919006600402915054906101000a900463ffffffff1681565b50600190565b6040518060400160405280600f81526020016e43727970746f43726561747572657360881b81525081565b600154600160a01b900460ff161561064c576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6106563382610f8e565b610681576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600081905260076020908152604080513381526001600160a01b03851692810192909252818101839052517f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259181900360600190a15050565b6001546001600160a01b03163314610713576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610748576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600180546001600160a01b0319166001600160a01b0392909216919091179055565b6004546000190190565b600154600160a01b900460ff16156107ad576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b0382166107e2576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b03821630141561081a576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6108243382610fae565b61084f576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6108593382610f8e565b610884576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b61088f838383610fce565b505050565b6001546001600160a01b031633146108cd576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600154600160a01b900460ff16610905576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b19169055565b6000546001600160a01b031633148061093757506001546001600160a01b031633145b61094057600080fd5b600154600160a01b900460ff1615610979576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001805460ff60a01b1916600160a01b179055565b6004818154811061099b57fe5b60009182526020909120600290910201805460019091015490915067ffffffffffffffff808216916801000000000000000081049091169063ffffffff600160801b8204811691600160a01b81049091169061ffff600160c01b9091041686565b60095481565b6000818152600560205260408120546001600160a01b0316610a45576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b506000908152600560205260409020546001600160a01b031690565b6001600160a01b031660009081526006602052604090205490565b606490565b6001546001600160a01b031681565b600154600160a01b900460ff1681565b600a805410610ad0576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6000610ae160008060008530610cd2565b6002549091506001600160a01b0316636528ab078230610aff610a7c565b604080516001600160e01b031960e087901b16815260048101949094526001600160a01b0390921660248401526001600160801b0316604483015262093a80606483015251608480830192600092919082900301818387803b158015610b6457600080fd5b505af1158015610b78573d6000803e3d6000fd5b5050600a8054600101905550505050565b60405180604001604052806002815260200161434b60f01b81525081565b62093a8081565b600060048281548110610bbd57fe5b6000918252602090912060029091020154600981905592915050565b600154600160a01b900460ff1615610c12576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038216610c47576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038216301415610c7f576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b610c893382610f8e565b610cb4576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b610cbf338383610fce565b5050565b6002546001600160a01b031681565b6000610cdc61109e565b6040518060c00160405280858152602001600067ffffffffffffffff1681526020014267ffffffffffffffff1681526020018863ffffffff1681526020018763ffffffff1681526020018661ffff16815250905060006001600483908060018154018082558091505090600182039060005260206000209060020201600090919290919091506000820151816000015560208201518160010160006101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060408201518160010160086101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060608201518160010160106101000a81548163ffffffff021916908363ffffffff16021790555060808201518160010160146101000a81548163ffffffff021916908363ffffffff16021790555060a08201518160010160186101000a81548161ffff021916908361ffff16021790555050500390507f58a3e712a89de27edeb9517f0adeb8aa16c75a478ac5197facfc848f82ec34f38482846000015160405180846001600160a01b03166001600160a01b03168152602001838152602001828152602001935050505060405180910390a1610ea960008583610fce565b979650505050505050565b6000546001600160a01b03163314610eed576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b6001600160a01b038116610f22576040805162461bcd60e51b8152602060048201526000602482015290519081900360640190fd5b600080546001600160a01b0319166001600160a01b0392909216919091179055565b6005602052600090815260409020546001600160a01b031681565b600a81565b6007602052600090815260409020546001600160a01b031681565b6000546001600160a01b031681565b6000908152600560205260409020546001600160a01b0391821691161490565b6000908152600760205260409020546001600160a01b0391821691161490565b6001600160a01b038083166000818152600660209081526040808320805460010190558583526005909152902080546001600160a01b031916909117905583161561104f576001600160a01b038316600090815260066020908152604080832080546000190190558383526007909152902080546001600160a01b03191690555b604080516001600160a01b0380861682528416602082015280820183905290517fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9181900360600190a1505050565b6040805160c081018252600080825260208201819052918101829052606081018290526080810182905260a08101919091529056fea265627a7a723058204b09eec351295acffb2fb5a0b7ecf107b40bfce8d7c0a9379df5f0cf8fe4896d64736f6c634300050a0032";
        public CreatureGeneratorDeploymentBase() : base(BYTECODE) { }
        public CreatureGeneratorDeploymentBase(string byteCode) : base(byteCode) { }

    }

    public partial class BreedingCooldownsFunction : BreedingCooldownsFunctionBase { }

    [Function("breedingCooldowns", "uint32")]
    public class BreedingCooldownsFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class SupportsInterfaceFunction : SupportsInterfaceFunctionBase { }

    [Function("supportsInterface", "bool")]
    public class SupportsInterfaceFunctionBase : FunctionMessage
    {
        [Parameter("bytes4", "_interfaceID", 1)]
        public virtual byte[] InterfaceID { get; set; }
    }

    public partial class NameFunction : NameFunctionBase { }

    [Function("name", "string")]
    public class NameFunctionBase : FunctionMessage
    {

    }

    public partial class ApproveFunction : ApproveFunctionBase { }

    [Function("approve")]
    public class ApproveFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 2)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class SetKingAddressFunction : SetKingAddressFunctionBase { }

    [Function("setKingAddress")]
    public class SetKingAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newKing", 1)]
        public virtual string NewKing { get; set; }
    }

    public partial class TotalSupplyFunction : TotalSupplyFunctionBase { }

    [Function("totalSupply", "uint256")]
    public class TotalSupplyFunctionBase : FunctionMessage
    {

    }

    public partial class TransferFromFunction : TransferFromFunctionBase { }

    [Function("transferFrom")]
    public class TransferFromFunctionBase : FunctionMessage
    {
        [Parameter("address", "_from", 1)]
        public virtual string From { get; set; }
        [Parameter("address", "_to", 2)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 3)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class UnpauseFunction : UnpauseFunctionBase { }

    [Function("unpause")]
    public class UnpauseFunctionBase : FunctionMessage
    {

    }

    public partial class PauseContractFunction : PauseContractFunctionBase { }

    [Function("pauseContract")]
    public class PauseContractFunctionBase : FunctionMessage
    {

    }

    public partial class CCreaturesFunction : CCreaturesFunctionBase { }

    [Function("cCreatures", typeof(CCreaturesOutputDTO))]
    public class CCreaturesFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class GenesOfCreatureFunction : GenesOfCreatureFunctionBase { }

    [Function("genesOfCreature", "uint256")]
    public class GenesOfCreatureFunctionBase : FunctionMessage
    {

    }

    public partial class OwnerOfFunction : OwnerOfFunctionBase { }

    [Function("ownerOf", "address")]
    public class OwnerOfFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class BalanceOfFunction : BalanceOfFunctionBase { }

    [Function("balanceOf", "uint256")]
    public class BalanceOfFunctionBase : FunctionMessage
    {
        [Parameter("address", "_owner", 1)]
        public virtual string Owner { get; set; }
    }

    public partial class Gen0PriceFunction : Gen0PriceFunctionBase { }

    [Function("gen0Price", "uint128")]
    public class Gen0PriceFunctionBase : FunctionMessage
    {

    }

    public partial class KingAddressFunction : KingAddressFunctionBase { }

    [Function("kingAddress", "address")]
    public class KingAddressFunctionBase : FunctionMessage
    {

    }

    public partial class ContractPausedFunction : ContractPausedFunctionBase { }

    [Function("contractPaused", "bool")]
    public class ContractPausedFunctionBase : FunctionMessage
    {

    }

    public partial class CreateGen0CreatureFunction : CreateGen0CreatureFunctionBase { }

    [Function("createGen0Creature")]
    public class CreateGen0CreatureFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_genes", 1)]
        public virtual BigInteger Genes { get; set; }
    }

    public partial class SymbolFunction : SymbolFunctionBase { }

    [Function("symbol", "string")]
    public class SymbolFunctionBase : FunctionMessage
    {

    }

    public partial class AUCTION_DURATIONFunction : AUCTION_DURATIONFunctionBase { }

    [Function("AUCTION_DURATION", "uint64")]
    public class AUCTION_DURATIONFunctionBase : FunctionMessage
    {

    }

    public partial class GetGenesFunction : GetGenesFunctionBase { }

    [Function("getGenes", "uint256")]
    public class GetGenesFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "tokenId", 1)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class TransferFunction : TransferFunctionBase { }

    [Function("transfer")]
    public class TransferFunctionBase : FunctionMessage
    {
        [Parameter("address", "_to", 1)]
        public virtual string To { get; set; }
        [Parameter("uint256", "_tokenId", 2)]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class MarketplaceFunction : MarketplaceFunctionBase { }

    [Function("marketplace", "address")]
    public class MarketplaceFunctionBase : FunctionMessage
    {

    }

    public partial class CreateCreatureFunction : CreateCreatureFunctionBase { }

    [Function("createCreature", "uint256")]
    public class CreateCreatureFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "_motherID", 1)]
        public virtual BigInteger MotherID { get; set; }
        [Parameter("uint256", "_fatherID", 2)]
        public virtual BigInteger FatherID { get; set; }
        [Parameter("uint256", "_generation", 3)]
        public virtual BigInteger Generation { get; set; }
        [Parameter("uint256", "_genes", 4)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("address", "_owner", 5)]
        public virtual string Owner { get; set; }
    }

    public partial class SetGodAddressFunction : SetGodAddressFunctionBase { }

    [Function("setGodAddress")]
    public class SetGodAddressFunctionBase : FunctionMessage
    {
        [Parameter("address", "_newGod", 1)]
        public virtual string NewGod { get; set; }
    }

    public partial class CreatureIndexToOwnerFunction : CreatureIndexToOwnerFunctionBase { }

    [Function("creatureIndexToOwner", "address")]
    public class CreatureIndexToOwnerFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class MAX_GEN0Function : MAX_GEN0FunctionBase { }

    [Function("MAX_GEN0", "uint256")]
    public class MAX_GEN0FunctionBase : FunctionMessage
    {

    }

    public partial class CreatureIndexToApprovedFunction : CreatureIndexToApprovedFunctionBase { }

    [Function("creatureIndexToApproved", "address")]
    public class CreatureIndexToApprovedFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class GodAddressFunction : GodAddressFunctionBase { }

    [Function("godAddress", "address")]
    public class GodAddressFunctionBase : FunctionMessage
    {

    }

    public partial class TransferEventDTO : TransferEventDTOBase { }

    [Event("Transfer")]
    public class TransferEventDTOBase : IEventDTO
    {
        [Parameter("address", "from", 1, false )]
        public virtual string From { get; set; }
        [Parameter("address", "to", 2, false )]
        public virtual string To { get; set; }
        [Parameter("uint256", "tokenId", 3, false )]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class ApprovalEventDTO : ApprovalEventDTOBase { }

    [Event("Approval")]
    public class ApprovalEventDTOBase : IEventDTO
    {
        [Parameter("address", "owner", 1, false )]
        public virtual string Owner { get; set; }
        [Parameter("address", "approved", 2, false )]
        public virtual string Approved { get; set; }
        [Parameter("uint256", "tokenId", 3, false )]
        public virtual BigInteger TokenId { get; set; }
    }

    public partial class CreatureCreatedEventDTO : CreatureCreatedEventDTOBase { }

    [Event("CreatureCreated")]
    public class CreatureCreatedEventDTOBase : IEventDTO
    {
        [Parameter("address", "owner", 1, false )]
        public virtual string Owner { get; set; }
        [Parameter("uint256", "creatureID", 2, false )]
        public virtual BigInteger CreatureID { get; set; }
        [Parameter("uint256", "genes", 3, false )]
        public virtual BigInteger Genes { get; set; }
    }

    public partial class BreedingCooldownsOutputDTO : BreedingCooldownsOutputDTOBase { }

    [FunctionOutput]
    public class BreedingCooldownsOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint32", "", 1)]
        public virtual uint ReturnValue1 { get; set; }
    }

    public partial class SupportsInterfaceOutputDTO : SupportsInterfaceOutputDTOBase { }

    [FunctionOutput]
    public class SupportsInterfaceOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }

    public partial class NameOutputDTO : NameOutputDTOBase { }

    [FunctionOutput]
    public class NameOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("string", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }





    public partial class TotalSupplyOutputDTO : TotalSupplyOutputDTOBase { }

    [FunctionOutput]
    public class TotalSupplyOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }







    public partial class CCreaturesOutputDTO : CCreaturesOutputDTOBase { }

    [FunctionOutput]
    public class CCreaturesOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "genes", 1)]
        public virtual BigInteger Genes { get; set; }
        [Parameter("uint64", "breedingCooldown", 2)]
        public virtual ulong BreedingCooldown { get; set; }
        [Parameter("uint64", "createdTime", 3)]
        public virtual ulong CreatedTime { get; set; }
        [Parameter("uint32", "motherID", 4)]
        public virtual uint MotherID { get; set; }
        [Parameter("uint32", "fatherID", 5)]
        public virtual uint FatherID { get; set; }
        [Parameter("uint16", "generation", 6)]
        public virtual ushort Generation { get; set; }
    }

    public partial class GenesOfCreatureOutputDTO : GenesOfCreatureOutputDTOBase { }

    [FunctionOutput]
    public class GenesOfCreatureOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class OwnerOfOutputDTO : OwnerOfOutputDTOBase { }

    [FunctionOutput]
    public class OwnerOfOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class BalanceOfOutputDTO : BalanceOfOutputDTOBase { }

    [FunctionOutput]
    public class BalanceOfOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "count", 1)]
        public virtual BigInteger Count { get; set; }
    }



    public partial class KingAddressOutputDTO : KingAddressOutputDTOBase { }

    [FunctionOutput]
    public class KingAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class ContractPausedOutputDTO : ContractPausedOutputDTOBase { }

    [FunctionOutput]
    public class ContractPausedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }



    public partial class SymbolOutputDTO : SymbolOutputDTOBase { }

    [FunctionOutput]
    public class SymbolOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("string", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class AUCTION_DURATIONOutputDTO : AUCTION_DURATIONOutputDTOBase { }

    [FunctionOutput]
    public class AUCTION_DURATIONOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint64", "", 1)]
        public virtual ulong ReturnValue1 { get; set; }
    }





    public partial class MarketplaceOutputDTO : MarketplaceOutputDTOBase { }

    [FunctionOutput]
    public class MarketplaceOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }





    public partial class CreatureIndexToOwnerOutputDTO : CreatureIndexToOwnerOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToOwnerOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class MAX_GEN0OutputDTO : MAX_GEN0OutputDTOBase { }

    [FunctionOutput]
    public class MAX_GEN0OutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }

    public partial class CreatureIndexToApprovedOutputDTO : CreatureIndexToApprovedOutputDTOBase { }

    [FunctionOutput]
    public class CreatureIndexToApprovedOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class GodAddressOutputDTO : GodAddressOutputDTOBase { }

    [FunctionOutput]
    public class GodAddressOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }
}
