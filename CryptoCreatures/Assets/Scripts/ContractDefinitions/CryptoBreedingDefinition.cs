using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.CryptoBreeding.ContractDefinition
{


    public partial class CryptoBreedingDeployment : CryptoBreedingDeploymentBase
    {
        public CryptoBreedingDeployment() : base(BYTECODE) { }
        public CryptoBreedingDeployment(string byteCode) : base(byteCode) { }
    }

    public class CryptoBreedingDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "6080604052348015600f57600080fd5b50603e80601d6000396000f3fe6080604052600080fdfea265627a7a72305820e7e87af7e3cda85fa49f2755846e6a41066fdc00162865f996c9c24c0f7eb1cb64736f6c634300050a0032";
        public CryptoBreedingDeploymentBase() : base(BYTECODE) { }
        public CryptoBreedingDeploymentBase(string byteCode) : base(byteCode) { }

    }
}
